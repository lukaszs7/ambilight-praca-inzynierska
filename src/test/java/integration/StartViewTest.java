package integration;

import com.szmolke.ambilight.algorithm.Screen;
import javafx.fxml.FXMLLoader;
import javafx.scene.Parent;
import javafx.scene.control.ChoiceBox;
import javafx.scene.control.Label;
import javafx.scene.control.Slider;
import org.junit.Assert;
import org.junit.Test;
import org.loadui.testfx.Assertions;
import org.loadui.testfx.GuiTest;
import org.loadui.testfx.controls.Commons;

import java.io.IOException;

/**
 * Created by Łukasz on 20.11.2016.
 */
public class StartViewTest extends GuiTest {
    @Override
    protected Parent getRootNode() {
        Parent parent = null;
        try {
            parent = FXMLLoader.load(getClass().getClassLoader().getResource("views/start.fxml"));
            return parent;
        } catch (IOException ex) {
            ex.printStackTrace();
        }
        return parent;
    }

    @Test
    public void choiceBoxTest() {
        //GIVEN
        ChoiceBox choiceMonitor = find("#monitorsList");

        //WHEN
        choiceMonitor.getSelectionModel().selectFirst();

        //THEN
        Assert.assertEquals(choiceMonitor.getSelectionModel().getSelectedItem(), Screen.PRIMARY);
    }

    @Test
    public void slidersAndLabelsTest() {
        //GIVEN
        Slider sliderRedBright = find("#sliderRedBright");
        Slider sliderRedContrast = find("#sliderRedContrast");
        Slider sliderRedGamma = find("#sliderRedGamma");
        Slider sliderGreenBright = find("#sliderGreenBright");
        Slider sliderGreenContrast = find("#sliderGreenContrast");
        Slider sliderGreenGamma = find("#sliderGreenGamma");
        Slider sliderBlueBright = find("#sliderBlueBright");
        Slider sliderBlueContrast = find("#sliderBlueContrast");
        Slider sliderBlueGamma = find("#sliderBlueGamma");

        //WHEN
        sliderRedBright.setValue(80);
        sliderRedContrast.setValue(0);
        sliderRedGamma.setValue(0.01);
        sliderGreenBright.setValue(80);
        sliderGreenContrast.setValue(0);
        sliderGreenGamma.setValue(0.01);
        sliderBlueBright.setValue(80);
        sliderBlueContrast.setValue(0);
        sliderBlueGamma.setValue(0.01);

        //THEN
        Assertions.verifyThat("#labelRedBright", Commons.hasText("80,00"));
        Assertions.verifyThat("#labelRedContrast", Commons.hasText("0,00"));
        Assertions.verifyThat("#labelRedGamma", Commons.hasText("0,01"));
        Assertions.verifyThat("#labelGreenBright", Commons.hasText("80,00"));
        Assertions.verifyThat("#labelGreenContrast", Commons.hasText("0,00"));
        Assertions.verifyThat("#labelGreenGamma", Commons.hasText("0,01"));
        Assertions.verifyThat("#labelBlueBright", Commons.hasText("80,00"));
        Assertions.verifyThat("#labelBlueContrast", Commons.hasText("0,00"));
        Assertions.verifyThat("#labelBlueGamma", Commons.hasText("0,01"));
    }
}
