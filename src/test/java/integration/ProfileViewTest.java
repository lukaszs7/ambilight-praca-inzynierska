package integration;

import com.szmolke.ambilight.controllers.ProfileController;
import com.szmolke.ambilight.repository.DatabaseConnector;
import javafx.collections.ObservableList;
import javafx.fxml.FXMLLoader;
import javafx.scene.Parent;
import javafx.scene.control.Button;
import javafx.scene.control.ChoiceBox;
import javafx.scene.control.TextField;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.loadui.testfx.Assertions;
import org.loadui.testfx.GuiTest;
import org.loadui.testfx.controls.Commons;

import java.io.IOException;

/**
 * Created by Łukasz on 20.11.2016.
 */
public class ProfileViewTest extends GuiTest {

    @Override
    protected Parent getRootNode() {
        Parent parent = null;
        try {
            parent = FXMLLoader.load(getClass().getClassLoader().getResource("views/profile.fxml"));
            return parent;
        } catch (IOException ex) {
            ex.printStackTrace();
        }
        return parent;
    }

    @Test
    public void checkInputs() {
        //GIVEN
        TextField profileName = find("#textNameProfile");

        //WHEN
        profileName.setText("profile1");

        //THEN
        Assertions.verifyThat("#textNameProfile", Commons.hasText("profile1"));
    }

    @Test
    public void checkChoiceBox() {
        //GIVEN
        ChoiceBox profileNames = find("#choiceProfile");
        profileNames.getItems().add("testProfile");
        boolean isExists = false;

        //WHEN
        ObservableList items = profileNames.getItems();
        for (int i = 0; i < items.size(); i++) {
            String nameItem = (String) items.get(i);
            if(nameItem.equals("testProfile")) {
                isExists = true;
                break;
            }
        }

        //THEN
        Assert.assertNotNull(profileNames);
        Assert.assertTrue(isExists);
    }
}
