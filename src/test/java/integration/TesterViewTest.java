package integration;

import javafx.fxml.FXMLLoader;
import javafx.scene.Parent;
import javafx.scene.control.Button;
import javafx.scene.layout.AnchorPane;
import javafx.scene.layout.StackPane;
import org.junit.Assert;
import org.junit.Test;
import org.loadui.testfx.GuiTest;

import java.io.IOException;

/**
 * Created by Łukasz on 20.11.2016.
 */
public class TesterViewTest extends GuiTest {

    @Override
    protected Parent getRootNode() {
        Parent parent = null;
        try {
            parent = FXMLLoader.load(getClass().getClassLoader().getResource("views/tester.fxml"));
            return parent;
        } catch (IOException ex) {
            ex.printStackTrace();
        }
        return parent;
    }

    @Test
    public void buttonShowSectorsTest() {
        //GIVEN
        Button showSectors = find("#showSectors");
        StackPane content = find("#canvasPane");

        //WHEN
        click(showSectors);

        //THEN
        Assert.assertTrue(showSectors.isVisible());
        Assert.assertNotNull(content.getChildren());
    }
}
