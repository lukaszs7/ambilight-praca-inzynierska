package unit.com.szmolke.ambilight.utils;

import com.szmolke.ambilight.utils.UtilsManager;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;

/**
 * Created by Łukasz on 28.11.2016.
 */
public class UtilsManagerTest {
    private UtilsManager utilsManager;

    @Before
    public void initUtilsManagerTest() {
        utilsManager = new UtilsManager();
    }

    @Test
    public void truncateDoubleTest() {
        //GIVEN
        double value1 = 0;
        double value2 = 0.01;

        //WHEN
        double result1 = utilsManager.truncateDouble(value1);
        double result2 = utilsManager.truncateDouble(value2);

        //THEN
        Assert.assertNotNull(result1);
        Assert.assertNotNull(result2);
        Assert.assertEquals(result1, 0.01, 2);
        Assert.assertEquals(result2, 0.01, 2);
    }

    @Test
    public void roundDoubleTest() {
        //GIVEN
        double value1 = 0.053432;
        double value2 = 0.0111111;

        //WHEN
        double result1 = utilsManager.roundDouble(value1, 2);
        double result2 = utilsManager.roundDouble(value2, 3);

        //THEN
        Assert.assertNotNull(result1);
        Assert.assertNotNull(result2);
        Assert.assertEquals(result1, 0.05, 2);
        Assert.assertEquals(result2, 0.011, 3);
    }
}
