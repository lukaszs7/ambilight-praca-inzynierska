package unit.com.szmolke.ambilight.utils;

import com.szmolke.ambilight.algorithm.Screen;
import com.szmolke.ambilight.utils.ScreenManager;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;

import java.awt.*;
import java.awt.image.BufferedImage;

/**
 * Created by Łukasz on 15.11.2016.
 */
public class ScreenManagerTest {
    private ScreenManager screenManager;

    @Before
    public void initScreenManager() {
        screenManager = new ScreenManager();
    }

    @Test
    public void getProperScreenTest() {
        //GIVEN
        int index = 1;
        int index2 = -1;
        int index3 = 2;

        //WHEN
        Screen screen1 = screenManager.getProperScreen(index);
        Screen screen2 = screenManager.getProperScreen(index2);
        Screen screen3 = screenManager.getProperScreen(index3);

        //THEN
        Assert.assertNotNull(screen1);
        Assert.assertEquals(screen1, Screen.SECONDARY);
        Assert.assertNull(screen2);
        Assert.assertNull(screen3);
    }

    @Test
    public void getProperScreenImageTest() {
        //GIVEN
        Screen screen = Screen.PRIMARY;
        Robot robot = screenManager.getRobot();

        //WHEN
        BufferedImage screenImage = screenManager.getProperScreenImage(screen, robot);

        //THEN
        Assert.assertNotNull(screenImage);
    }
}
