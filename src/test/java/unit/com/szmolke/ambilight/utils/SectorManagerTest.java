package unit.com.szmolke.ambilight.utils;

import com.szmolke.ambilight.algorithm.Sector;
import com.szmolke.ambilight.utils.SectorManager;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;

/**
 * Created by Łukasz on 15.11.2016.
 */
public class SectorManagerTest {
    private SectorManager sectorManager;

    @Before
    public void initSectorManager() {
        sectorManager = new SectorManager();
    }

    @Test
    public void getSectorsTest() {
        //GIVEN
        int[] ledsCount = new int[]{1, 2, 3, 0};
        double imageW = 1920;
        double imageH = 1080;
        int sectorW = 100;
        int sectorH = 50;

        //WHEN
        Sector[] sectors = sectorManager.getSectors(imageW, imageH, sectorW, sectorH, ledsCount);

        //THEN
        Assert.assertNotNull(sectors);
        Assert.assertEquals(sectors.length, 6);
        Assert.assertEquals(sectors[2].getLed(), 3);
        Assert.assertEquals(sectors[5].getLed(), 6);
    }

    @Test
    public void concatListRectTest() {
        //GIVEN
        int[] ledsCount = new int[]{1, 2, 3, 0};
        double imageW = 1920;
        double imageH = 1080;
        int sectorW = 100;
        int sectorH = 50;
        Sector[] sectors1 = sectorManager.getSectors(imageW, imageH, sectorW, sectorH, ledsCount);
        Sector[] sectors2 = sectorManager.getSectors(imageW, imageH, sectorW, sectorH, ledsCount);

        //WHEN
        Sector[] concatSectors = sectorManager.concatListRects(sectors1, sectors2);

        //THEN
        Assert.assertNotNull(concatSectors);
        Assert.assertEquals(concatSectors.length, sectors1.length + sectors2.length);
        Assert.assertEquals(concatSectors[0], sectors1[0]);
        Assert.assertEquals(concatSectors[sectors1.length], sectors2[0]);
    }
}
