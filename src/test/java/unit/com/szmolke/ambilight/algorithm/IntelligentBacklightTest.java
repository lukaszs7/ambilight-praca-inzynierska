package unit.com.szmolke.ambilight.algorithm;

import com.szmolke.ambilight.algorithm.Configuration;
import com.szmolke.ambilight.algorithm.IntelligentBacklight;
import com.szmolke.ambilight.algorithm.Screen;
import com.szmolke.ambilight.algorithm.Sector;
import com.szmolke.ambilight.utils.SectorManager;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;

import java.awt.*;

/**
 * Created by Łukasz on 15.11.2016.
 */
public class IntelligentBacklightTest {

//    private SectorManager sectorManager;
//
//    @Before
//    public void initBacklight() {
//
//    }

//    @Test
//    public void setSerialPortRunningTest() {
//        //GIVEN
//        String portName = Configuration.PORT_NAME;
//        int baudrate = Configuration.getBaudrate(9);
//        int delay = 50;
//        Sector[] sectors = new Sector[]{new Sector(0,0, 50, 50)};
//
//        //WHEN
//        IntelligentBacklight intelligentBacklight = new IntelligentBacklight(portName, baudrate, Screen.PRIMARY, delay, sectors);
//        if(portName != "") {
//            intelligentBacklight.setSerialPortRunning(true);
//        } else {
//            intelligentBacklight.setSerialPortRunning(false);
//        }
//
//        //THEN
//        Assert.assertNotNull(intelligentBacklight);
//    }

    @Test
    public void manageBrithnessColorTest() {
        //GIVEN
        int brightnessRed = 30;
        int brightnessGreen = 100;
        int brightnessBlue = 80;
        Color sourceColor = new Color(255, 100, 50);

        //WHEN
        Color newColor = IntelligentBacklight.manageBrithnessColor(sourceColor, brightnessRed, brightnessGreen, brightnessBlue);

        //THEN
        Assert.assertNotNull(newColor);
        Assert.assertEquals(newColor.getRed(), 76);
        Assert.assertEquals(newColor.getGreen(), 100);
        Assert.assertEquals(newColor.getBlue(), 40);
    }

    @Test
    public void manageContrastCorrectionTest() {
        //GIVEN
        double contrastRed = -58.80;
        double contrastGreen = 120;
        double contrastBlue = 3.8231;
        Color sourceColor = new Color(255, 100, 50);

        //WHEN
        Color newColor = IntelligentBacklight.manageContrastCorrection(sourceColor, contrastRed, contrastGreen, contrastBlue);

        //THEN
        Assert.assertNotNull(newColor);
        Assert.assertEquals(newColor.getRed(), 207);
        Assert.assertEquals(newColor.getGreen(), 51);
        Assert.assertEquals(newColor.getBlue(), 47);
    }

    @Test
    public void manageGammaCorrectionTest() {
        //GIVEN
        double gammaRed = 0.32;
        double gammaGreen = 4.82;
        double gammaBlue = 2;
        Color sourceColor = new Color(255, 100, 50);

        //WHEN
        Color newColor = IntelligentBacklight.manageGammaCorrection(sourceColor, gammaRed, gammaGreen, gammaBlue);

        //THEN
        Assert.assertNotNull(newColor);
        Assert.assertEquals(newColor.getRed(), 255);
        Assert.assertEquals(newColor.getGreen(), 209);
        Assert.assertEquals(newColor.getBlue(), 112);
    }
}
