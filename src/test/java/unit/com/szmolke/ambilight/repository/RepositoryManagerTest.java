package unit.com.szmolke.ambilight.repository;

import com.szmolke.ambilight.algorithm.Configuration;
import com.szmolke.ambilight.repository.ColorEntity;
import com.szmolke.ambilight.repository.RepositoryManager;
import com.szmolke.ambilight.repository.SectorEntity;
import com.szmolke.ambilight.repository.SettingsEntity;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;

import java.util.List;

/**
 * Created by Łukasz on 28.11.2016.
 */
public class RepositoryManagerTest {
    private RepositoryManager repositoryManager;

    @Before
    public void initTests() {
        repositoryManager = new RepositoryManager();
    }

    @Test
    public void createProperSettingsEntityTest() {
        //WHEN
        String textNameProfile = "test";

        //GIVEN
        SettingsEntity settingsEntity = repositoryManager.createProperSettingsEntity(textNameProfile);

        //THEN
        Assert.assertNotNull(settingsEntity);
        Assert.assertEquals(settingsEntity.getProfileName(), textNameProfile);
        Assert.assertEquals(settingsEntity.getBaudrate(), Configuration.BAUDRATE);
        Assert.assertEquals(settingsEntity.getInitDelay(), Configuration.INIT_DELAY);
        Assert.assertEquals(settingsEntity.getInitLedsBottom(), Configuration.INIT_LEDS_BOTTOM);
        Assert.assertEquals(settingsEntity.getInitLedsLeft(), Configuration.INIT_LEDS_LEFT);
        Assert.assertEquals(settingsEntity.getInitLedsRight(), Configuration.INIT_LEDS_RIGHT);
        Assert.assertEquals(settingsEntity.getInitLedsTop(), Configuration.INIT_LEDS_TOP);
        Assert.assertEquals(settingsEntity.getMode(), Configuration.MODE);
        Assert.assertEquals(settingsEntity.getSerialPort(), Configuration.PORT_NAME);
    }

    @Test
    public void createColorEntitySettingsTest() {
        //WHEN
        //GIVEN
        ColorEntity colorEntity = repositoryManager.createColorEntitySettings();

        //THEN
        Assert.assertNotNull(colorEntity);
        Assert.assertEquals(colorEntity.getRedBright(), Configuration.RED_BRIGHT);
        Assert.assertEquals(colorEntity.getBlueBright(), Configuration.BLUE_BRIGHT);
        Assert.assertEquals(colorEntity.getGreenBright(), Configuration.GREEN_BRIGHT);
        Assert.assertEquals(colorEntity.getRedContrast(), Configuration.RED_CONTRAST, 2);
        Assert.assertEquals(colorEntity.getGreenContrast(), Configuration.GREEN_CONTRAST, 2);
        Assert.assertEquals(colorEntity.getBlueContrast(), Configuration.BLUE_CONTRAST, 2);
        Assert.assertEquals(colorEntity.getRedGamma(), Configuration.RED_GAMMA, 2);
        Assert.assertEquals(colorEntity.getGreenGamma(), Configuration.GREEN_GAMMA, 2);
        Assert.assertEquals(colorEntity.getBlueGamma(), Configuration.BLUE_GAMMA, 2);
    }

    @Test
    public void createSectorEntitiesSettingsTest() {
        //WHEN
        String profileName = "Test";
        SettingsEntity settingsEntity = repositoryManager.createProperSettingsEntity(profileName);

        //GIVEN
        List<SectorEntity> sectorEntities = repositoryManager.createSectorEntitiesSettings(settingsEntity);

        //THEN
        Assert.assertNotNull(sectorEntities);
        Assert.assertEquals(sectorEntities.size(), Configuration.INIT_LEDS_TOP + Configuration.INIT_LEDS_LEFT + Configuration.INIT_LEDS_BOTTOM + Configuration.INIT_LEDS_RIGHT);
    }
}
