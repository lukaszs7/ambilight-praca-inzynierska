package unit.com.szmolke.ambilight.repository;

import com.szmolke.ambilight.repository.DatabaseConnector;
import com.szmolke.ambilight.repository.RepositoryManager;
import com.szmolke.ambilight.repository.SectorEntity;
import com.szmolke.ambilight.repository.SettingsEntity;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;

import java.util.List;

/**
 * Created by Łukasz on 14.11.2016.
 */
public class DatabaseConnectorTest {

    DatabaseConnector databaseConnector;
    RepositoryManager repositoryManager;

    @Before
    public void initTester() {
        databaseConnector = new DatabaseConnector();
        repositoryManager = new RepositoryManager();
    }

    @Test
    public void createSectorTest() {
        //GIVEN
        SectorEntity sectorEntity = new SectorEntity();
        sectorEntity.setLed(1);
        sectorEntity.setWidth(0);
        sectorEntity.setHeight(0);

        //WHEN
        long id = databaseConnector.createEntity(sectorEntity);
        SectorEntity fromDB = databaseConnector.findSectorById(id);
        if(fromDB != null) {
            databaseConnector.deleteSectorById(fromDB);
        }

        //THEN
        Assert.assertNotNull(fromDB);
        Assert.assertEquals(new Long(id), new Long(fromDB.getId()));
    }

    @Test
    public void deleteProfileTest() {
        //GIVEN
        String profileName = "test";
        SettingsEntity profile = repositoryManager.createProperSettingsEntity(profileName);
        List<SectorEntity> sectors = repositoryManager.createSectorEntitiesSettings(profile);

        //WHEN
        if (!databaseConnector.isProfileExists(profile) && sectors.size() > 0) {
            databaseConnector.createEntity(sectors.get(0));
        }
        databaseConnector.deleteProfile(profile);
        boolean exists = databaseConnector.isProfileExists(profile);

        //THEN
        Assert.assertFalse(exists);
    }

    @Test
    public void isProfileExistsTest() {
        //GIVEN
        String profileName = "test";
        SettingsEntity profile = repositoryManager.createProperSettingsEntity(profileName);
        List<SectorEntity> sectors = repositoryManager.createSectorEntitiesSettings(profile);

        //WHEN
        databaseConnector.createEntity(sectors.get(0));
        boolean exists = databaseConnector.isProfileExists(profile);
        databaseConnector.deleteProfile(profile);

        //THEN
        Assert.assertTrue(exists);
    }
}
