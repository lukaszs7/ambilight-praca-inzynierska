package com.szmolke.ambilight.repository;

import javax.persistence.*;

/**
 * Created by Łukasz on 11.11.2016.
 */

@Entity
@Table(schema = "PUBLIC")
@NamedQueries({
        @NamedQuery(name = "ColorEntity.updateColorsById", query = "UPDATE ColorEntity CE " +
                "SET CE.blueBright = :blueBright, CE.blueContrast = :blueContrast, CE.blueGamma = :blueGamma," +
                "CE.greenBright = :greenBright, CE.greenContrast = :greenContrast, CE.greenGamma = :greenGamma," +
                "CE.redBright = :redBright, CE.redContrast = :redContrast, CE.redGamma = :redGamma WHERE CE.id = :color_id"),
        @NamedQuery(name = "ColorEntity.deleteById", query = "DELETE FROM ColorEntity CE WHERE CE.id = :color_id")
})
public class ColorEntity {
    public static final String COLOR_ID_PARAM = "color_id";

    @Id
    @Column(name = "color_id")
    @GeneratedValue(strategy = GenerationType.AUTO)
    private Long id;
    @Column(name = "red_bright")
    private int redBright;
    @Column(name = "green_bright")
    private int greenBright;
    @Column(name = "blue_bright")
    private int blueBright;
    @Column(name = "red_contrast")
    private double redContrast;
    @Column(name = "green_contrast")
    private double greenContrast;
    @Column(name = "blue_contrast")
    private double blueContrast;
    @Column(name = "red_gamma")
    private double redGamma;
    @Column(name = "green_gamma")
    private double greenGamma;
    @Column(name = "blue_gamma")
    private double blueGamma;

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public int getRedBright() {
        return redBright;
    }

    public void setRedBright(int redBright) {
        this.redBright = redBright;
    }

    public int getGreenBright() {
        return greenBright;
    }

    public void setGreenBright(int greenBright) {
        this.greenBright = greenBright;
    }

    public int getBlueBright() {
        return blueBright;
    }

    public void setBlueBright(int blueBright) {
        this.blueBright = blueBright;
    }

    public double getRedContrast() {
        return redContrast;
    }

    public void setRedContrast(double redContrast) {
        this.redContrast = redContrast;
    }

    public double getGreenContrast() {
        return greenContrast;
    }

    public void setGreenContrast(double greenContrast) {
        this.greenContrast = greenContrast;
    }

    public double getBlueContrast() {
        return blueContrast;
    }

    public void setBlueContrast(double blueContrast) {
        this.blueContrast = blueContrast;
    }

    public double getRedGamma() {
        return redGamma;
    }

    public void setRedGamma(double redGamma) {
        this.redGamma = redGamma;
    }

    public double getGreenGamma() {
        return greenGamma;
    }

    public void setGreenGamma(double greenGamma) {
        this.greenGamma = greenGamma;
    }

    public double getBlueGamma() {
        return blueGamma;
    }

    public void setBlueGamma(double blueGamma) {
        this.blueGamma = blueGamma;
    }

    @Override
    public String toString() {
        return "ColorEntity{" +
                "id=" + id +
                ", redBright=" + redBright +
                ", greenBright=" + greenBright +
                ", blueBright=" + blueBright +
                ", redContrast=" + redContrast +
                ", greenContrast=" + greenContrast +
                ", blueContrast=" + blueContrast +
                ", redGamma=" + redGamma +
                ", greenGamma=" + greenGamma +
                ", blueGamma=" + blueGamma +
                '}';
    }
}
