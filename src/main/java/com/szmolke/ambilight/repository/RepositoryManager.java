package com.szmolke.ambilight.repository;

import com.szmolke.ambilight.algorithm.Configuration;
import com.szmolke.ambilight.algorithm.Sector;
import com.szmolke.ambilight.utils.SectorManager;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by Łukasz on 16.11.2016.
 */
public class RepositoryManager {
    private static final Logger log = LoggerFactory.getLogger(RepositoryManager.class);

    public SettingsEntity createProperSettingsEntity(String textNameProfile) {
        SettingsEntity entity = new SettingsEntity();
        entity.setProfileName(textNameProfile);
        entity.setSerialPort(Configuration.PORT_NAME);
        entity.setBaudrate(Configuration.BAUDRATE);
        entity.setInitDelay(Configuration.INIT_DELAY);
        entity.setMonitor(Configuration.MONITOR);
        entity.setClockwise(Configuration.DIRECTION_CLOCKWISE);
        entity.setMode(Configuration.MODE);
        entity.setInitLedsTop(Configuration.INIT_LEDS_TOP);
        entity.setInitLedsRight(Configuration.INIT_LEDS_RIGHT);
        entity.setInitLedsBottom(Configuration.INIT_LEDS_BOTTOM);
        entity.setInitLedsLeft(Configuration.INIT_LEDS_LEFT);
        entity.setColors(createColorEntitySettings());
        return entity;
    }

    public ColorEntity createColorEntitySettings() {
        ColorEntity colorEntity = new ColorEntity();
        colorEntity.setRedBright(Configuration.RED_BRIGHT);
        colorEntity.setBlueBright(Configuration.BLUE_BRIGHT);
        colorEntity.setGreenBright(Configuration.GREEN_BRIGHT);
        colorEntity.setRedContrast(Configuration.RED_CONTRAST);
        colorEntity.setGreenContrast(Configuration.GREEN_CONTRAST);
        colorEntity.setBlueContrast(Configuration.BLUE_CONTRAST);
        colorEntity.setRedGamma(Configuration.RED_GAMMA);
        colorEntity.setGreenGamma(Configuration.GREEN_GAMMA);
        colorEntity.setBlueGamma(Configuration.BLUE_GAMMA);
        return colorEntity;
    }

    public List<SectorEntity> createSectorEntitiesSettings(SettingsEntity settingsEntity) {
        List<SectorEntity> sectorsSet = new ArrayList<>();
        SectorManager sectorManager = new SectorManager();
        Sector[] arraySectors = sectorManager.getSectors();
        for(Sector sector : arraySectors) {
            SectorEntity entity = new SectorEntity();
            entity.setSkip(sector.getSkip());
            entity.setLed(sector.getLed());
            entity.setWidth((int)sector.getWidth());
            entity.setHeight((int)sector.getHeight());
            entity.setSettingsEntity(settingsEntity);
            sectorsSet.add(entity);
        }
        return sectorsSet;
    }
}
