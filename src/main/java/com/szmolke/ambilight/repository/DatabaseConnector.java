package com.szmolke.ambilight.repository;

import com.szmolke.ambilight.algorithm.Configuration;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import javax.persistence.EntityManager;
import javax.persistence.Persistence;
import java.util.List;

/**
 * Created by Łukasz on 28.10.2016.
 */
public class DatabaseConnector {
    private final EntityManager entityManager = Persistence.createEntityManagerFactory("unit").createEntityManager();
    private static final Logger log = LoggerFactory.getLogger(DatabaseConnector.class);

    public Long createEntity(SectorEntity entity) {
        entityManager.getTransaction().begin();
        log.info("START creating entity");
        entityManager.persist(entity);
        log.info("STOP creating entity");
        entityManager.getTransaction().commit();
        return entity.getId();
    }

    public SectorEntity findSectorById(Long id) {
        entityManager.getTransaction().begin();
        log.info("START query SectorEntity");
        SectorEntity sector = entityManager.find(SectorEntity.class, id);
        entityManager.getTransaction().commit();
        log.info("STOP query SectorEntity");
        return sector;
    }

    public void deleteSectorById(SectorEntity entity) {
        entityManager.getTransaction().begin();
        log.info("START delete SectorEntity");
        entityManager.remove(entity);
        entityManager.getTransaction().commit();
        log.info("STOP delete SectorEntity");
    }

    public boolean isProfileExists(SettingsEntity profileName) {
        entityManager.getTransaction().begin();
        log.info("START isProfileExists");
        boolean exists = entityManager
                .createNamedQuery("SettingsEntity.countSettingsProfile", Long.class)
                .setParameter(SettingsEntity.PROFILE_NAME_ID, profileName.getProfileName())
                .getSingleResult() > 0L;
        entityManager.getTransaction().commit();
        log.info("STOP isProfileExists: "+exists);
        return exists;
    }

    public SettingsEntity findSettingsByProfileName(String profileName) {
        entityManager.getTransaction().begin();
        log.info("START findSettingsByProfileName");
        SettingsEntity settingsEntity = entityManager
                .createNamedQuery("SettingsEntity.getByProfileName", SettingsEntity.class)
                .setParameter(SettingsEntity.PROFILE_NAME_ID, profileName)
                .getSingleResult();
        entityManager.getTransaction().commit();
        log.info("STOP findSettingsByProfileName: "+settingsEntity);
        return settingsEntity;
    }

    public void deleteProfile(SettingsEntity toDelete) {
        deleteSectorsWithSetId(toDelete.getId());
        deleteSettinsEntity(toDelete);
        deleteColorEntity(toDelete.getColors());
    }

    public void deleteSectorsWithSetId(Long settingsId) {
        entityManager.getTransaction().begin();
        log.info("START deleting sectors by SettingsID: "+settingsId);
        entityManager.createNamedQuery("SectorEntity.deleteBySetId")
                .setParameter(SectorEntity.QUERY_SETTINGS_ID, settingsId)
                .executeUpdate();
        log.info("STOP deleting sectors");
        entityManager.getTransaction().commit();
    }

    public void deleteColorEntity(ColorEntity colorEntity) {
        entityManager.getTransaction().begin();
        log.info("START deleting colorEntity: "+colorEntity.getId());
        entityManager.createNamedQuery("ColorEntity.deleteById")
                .setParameter(ColorEntity.COLOR_ID_PARAM, colorEntity.getId())
                .executeUpdate();
        log.info("STOP deleting colorEntity");
        entityManager.getTransaction().commit();
    }

    public void deleteSettinsEntity(SettingsEntity settingsEntity) {
        entityManager.getTransaction().begin();
        log.info("START deleting settingsEntity: "+settingsEntity.getId());
        entityManager.createNamedQuery("SettingsEntity.deleteById")
                .setParameter(SettingsEntity.SETTINGS_ID_PARAM, settingsEntity.getId())
                .executeUpdate();
        log.info("STOP deleting settingsEntity");
        entityManager.getTransaction().commit();
    }

    public void updateColorEntity(Long colorId, ColorEntity source) {
        entityManager.getTransaction().begin();
        log.info("START updating colors by colorID: "+colorId);
        entityManager.createNamedQuery("ColorEntity.updateColorsById")
                .setParameter("blueBright", source.getBlueBright()).setParameter("blueContrast", source.getBlueContrast())
                .setParameter("blueGamma", source.getBlueGamma()).setParameter("greenBright", source.getGreenBright())
                .setParameter("greenContrast", source.getGreenContrast()).setParameter("greenGamma", source.getGreenGamma())
                .setParameter("redBright", source.getRedBright()).setParameter("redContrast", source.getRedContrast())
                .setParameter("redGamma", source.getRedGamma()).setParameter("color_id", colorId).executeUpdate();
        entityManager.getTransaction().commit();
    }

    public void updateSettingsEntity(Long settingsId, Long colorsId, SettingsEntity sourceSettings) {
        entityManager.getTransaction().begin();
        log.info("START updating settings by settingsID: "+settingsId);
        entityManager.createNamedQuery("SettingsEntity.updateSettingsById")
                .setParameter("baudrate", sourceSettings.getBaudrate()).setParameter("clockwise", sourceSettings.isClockwise())
                .setParameter("initDelay", sourceSettings.getInitDelay()).setParameter("initLedsTop", sourceSettings.getInitLedsTop())
                .setParameter("initLedsRight", sourceSettings.getInitLedsRight()).setParameter("initLedsBottom", sourceSettings.getInitLedsBottom())
                .setParameter("initLedsLeft", sourceSettings.getInitLedsLeft()).setParameter("serialPort", sourceSettings.getSerialPort())
                .setParameter("monitor", sourceSettings.getMonitor()).setParameter("mode", sourceSettings.getMode())
                .setParameter("colors_id", colorsId).setParameter("settings_id", settingsId).executeUpdate();
        entityManager.getTransaction().commit();
    }

    public List<String> findAllProfileNames() {
        entityManager.getTransaction().begin();
        log.info("START findAllProfiles");
        List<String> names = entityManager
                .createNamedQuery("SettingsEntity.selectProfileNames", String.class)
                .getResultList();
        entityManager.getTransaction().commit();
        log.info("STOP findAllProfiles");
        return names;
    }

    public void closeEntityManager() {
        entityManager.close();
    }
}
