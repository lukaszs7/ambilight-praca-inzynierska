package com.szmolke.ambilight.repository;

import com.szmolke.ambilight.algorithm.Mode;
import com.szmolke.ambilight.algorithm.Screen;

import javax.persistence.*;
import java.util.Set;

/**
 * Created by Łukasz on 27.10.2016.
 */

@Entity
@Table(schema = "PUBLIC")
@NamedQueries({
        @NamedQuery(name = "SettingsEntity.countSettingsProfile", query = "SELECT count(*) FROM SettingsEntity SE WHERE profileName = :profileName"),
        @NamedQuery(name = "SettingsEntity.getByProfileName", query = "SELECT SE FROM SettingsEntity SE WHERE SE.profileName = :profileName"),
        @NamedQuery(name = "SettingsEntity.updateSettingsById", query = "" +
                "UPDATE SettingsEntity SE SET SE.baudrate = :baudrate, SE.clockwise = :clockwise," +
                "SE.initDelay = :initDelay, SE.initLedsBottom = :initLedsBottom, SE.initLedsLeft = :initLedsLeft," +
                "SE.initLedsRight = :initLedsRight, SE.initLedsTop = :initLedsTop, SE.mode = :mode, SE.serialPort = :serialPort," +
                "SE.monitor = :monitor, SE.colors.id = :colors_id WHERE SE.id = :settings_id"),
        @NamedQuery(name = "SettingsEntity.selectProfileNames", query = "SELECT SE.profileName FROM SettingsEntity SE"),
        @NamedQuery(name = "SettingsEntity.deleteById", query = "DELETE FROM SettingsEntity SE WHERE SE.id = :settings_id")
})
public class SettingsEntity {
    public static final String PROFILE_NAME_ID = "profileName";
    public static final String SETTINGS_ID_PARAM = "settings_id";

    @Id
    @Column(name = "settings_id")
    @GeneratedValue(strategy = GenerationType.AUTO)
    private Long id;
    @Column(name = "PROFILE_NAME", unique = true, nullable = false)
    private String profileName;
    @Column(name = "INIT_DELAY")
    private int initDelay;
    @Column(name = "INIT_LEDS_TOP")
    private int initLedsTop;
    @Column(name = "INIT_LEDS_RIGHT")
    private int initLedsRight;
    @Column(name = "INIT_LEDS_BOTTOM")
    private int initLedsBottom;
    @Column(name = "INIT_LEDS_LEFT")
    private int initLedsLeft;
    @Column
    private Screen monitor;
    @Column
    private Mode mode;
    @Column
    private int baudrate;
    @Column(name = "INIT_SERIAL_PORT")
    private String serialPort;
    @Column
    private boolean clockwise;
    @OneToMany(mappedBy = "settingsEntity")
    private Set<SectorEntity> sectors;
    @OneToOne(cascade = {CascadeType.ALL})
    @JoinColumn(name = "color_id")
    private ColorEntity colors;

    public String getProfileName() {
        return profileName;
    }

    public void setProfileName(String profileName) {
        this.profileName = profileName;
    }

    public int getInitDelay() {
        return initDelay;
    }

    public void setInitDelay(int initDelay) {
        this.initDelay = initDelay;
    }

    public int getInitLedsTop() {
        return initLedsTop;
    }

    public void setInitLedsTop(int initLedsTop) {
        this.initLedsTop = initLedsTop;
    }

    public int getInitLedsRight() {
        return initLedsRight;
    }

    public void setInitLedsRight(int initLedsRight) {
        this.initLedsRight = initLedsRight;
    }

    public int getInitLedsBottom() {
        return initLedsBottom;
    }

    public void setInitLedsBottom(int initLedsBottom) {
        this.initLedsBottom = initLedsBottom;
    }

    public int getInitLedsLeft() {
        return initLedsLeft;
    }

    public void setInitLedsLeft(int initLedsLeft) {
        this.initLedsLeft = initLedsLeft;
    }

    public Set<SectorEntity> getSectors() {
        return sectors;
    }

    public void setSectors(Set<SectorEntity> sectors) {
        this.sectors = sectors;
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public Screen getMonitor() {
        return monitor;
    }

    public void setMonitor(Screen monitor) {
        this.monitor = monitor;
    }

    public int getBaudrate() {
        return baudrate;
    }

    public void setBaudrate(int baudrate) {
        this.baudrate = baudrate;
    }

    public String getSerialPort() {
        return serialPort;
    }

    public void setSerialPort(String serialPort) {
        this.serialPort = serialPort;
    }

    public boolean isClockwise() {
        return clockwise;
    }

    public void setClockwise(boolean clockwise) {
        this.clockwise = clockwise;
    }

    public ColorEntity getColors() {
        return colors;
    }

    public void setColors(ColorEntity colors) {
        this.colors = colors;
    }

    public Mode getMode() {
        return mode;
    }

    public void setMode(Mode mode) {
        this.mode = mode;
    }


    @Override
    public String toString() {
        return "SettingsEntity{" +
                "id=" + id +
                ", profileName='" + profileName + '\'' +
                ", initDelay=" + initDelay +
                ", initLedsTop=" + initLedsTop +
                ", initLedsRight=" + initLedsRight +
                ", initLedsBottom=" + initLedsBottom +
                ", initLedsLeft=" + initLedsLeft +
                ", monitor=" + monitor +
                ", mode=" + mode +
                ", baudrate=" + baudrate +
                ", serialPort='" + serialPort + '\'' +
                ", clockwise=" + clockwise +
                ", sectors=" + sectors +
                ", colors=" + colors +
                '}';
    }
}
