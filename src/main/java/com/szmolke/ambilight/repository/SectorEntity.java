package com.szmolke.ambilight.repository;

import javax.persistence.*;

/**
 * Created by Łukasz on 28.10.2016.
 */

@Entity
@Table(schema = "PUBLIC")
@NamedQueries({
        @NamedQuery(name = "SectorEntity.getById", query = "SELECT SE FROM SectorEntity SE WHERE SE.id = :sector_id"),
        @NamedQuery(name = "SectorEntity.deleteById", query = "DELETE FROM SectorEntity SE WHERE SE.id = :sector_id"),
        @NamedQuery(name = "SectorEntity.deleteBySetId", query = "DELETE FROM SectorEntity SE WHERE SE.settingsEntity.id = :settings_id")
})
public class SectorEntity {
    public static final String QUERY_SECTOR_ID = "sector_id";
    public static final String QUERY_SETTINGS_ID = "settings_id";

    @Id
    @Column(name = "sector_id")
    @GeneratedValue(strategy = GenerationType.AUTO)
    private Long id;
    @Column(name = "led_number")
    private int led;
    @Column(name = "sector_width")
    private int width;
    @Column(name = "sector_height")
    private int height;
    @Column(name = "sector_skip")
    private int skip;
    @ManyToOne(fetch=FetchType.LAZY, cascade = {CascadeType.ALL})
    @JoinColumn(name="settings_id")
    private SettingsEntity settingsEntity;

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public int getWidth() {
        return width;
    }

    public void setWidth(int width) {
        this.width = width;
    }

    public int getHeight() {
        return height;
    }

    public void setHeight(int height) {
        this.height = height;
    }

    public int getSkip() {
        return skip;
    }

    public void setSkip(int skip) {
        this.skip = skip;
    }

    public int getLed() {
        return led;
    }

    public void setLed(int led) {
        this.led = led;
    }

    public SettingsEntity getSettingsEntity() {
        return settingsEntity;
    }

    public void setSettingsEntity(SettingsEntity settingsEntity) {
        this.settingsEntity = settingsEntity;
    }

    @Override
    public String toString() {
        return "SectorEntity{" +
                "id=" + id +
                ", led=" + led +
                ", width=" + width +
                ", height=" + height +
                ", skip=" + skip +
                '}';
    }
}
