package com.szmolke.ambilight.algorithm;

/**
 * Created by Łukasz on 07.08.2016.
 */
public enum Screen {
    PRIMARY("Pierwszy"), SECONDARY("Drugi");

    private String text;

    Screen(String text) {
        this.text = text;
    }

    public static String[] getTranslateValues() {
        String[] values = new String[Mode.values().length];
        for(int i = 0; i < values.length; i++) {
            values[i] = Mode.values()[i].getText();
        }
        return values;
    }

    public String getText() {
        return text;
    }
}
