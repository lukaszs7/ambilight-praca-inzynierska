package com.szmolke.ambilight.algorithm;

/**
 * Created by Łukasz on 11.11.2016.
 */
public enum Mode {
    DYNAMIC(Configuration.MODE_DYNAMIC), STATIC(Configuration.MODE_STATIC);

    private String text;

    Mode(String text) {
        this.text = text;
    }

    public static String[] getTranslateValues() {
        String[] values = new String[Mode.values().length];
        for(int i = 0; i < values.length; i++) {
            values[i] = Mode.values()[i].getText();
        }
        return values;
    }

    public String getText() {
        return text;
    }
}
