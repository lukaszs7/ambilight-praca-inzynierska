package com.szmolke.ambilight.algorithm;

import javafx.scene.shape.Rectangle;

import java.awt.*;
import java.awt.image.BufferedImage;

/**
 * Created by Łukasz on 02.08.2016.
 */
public class Sector extends Rectangle {
    private int led;
    private int skip;
    private boolean startSector = false;
    private boolean vertical = false;

    public Sector(double x, double y, double width, double height, boolean vertical) {
        super(x, y, width, height);
        this.vertical = vertical;
    }

    public Color getAvgColor(BufferedImage fullScreen) {
        int r = 0;
        int g = 0;
        int b = 0;
        int loops = 0;
        Color color;
        int sectorX = (int)this.getX();
        int sectorY = (int)this.getY();
        int sectorW = (int)this.getWidth();
        int sectorH = (int)this.getHeight();

        for (int i = sectorX; i < (sectorX + sectorW); i += Configuration.SECTOR_SKIP) {
            for (int j = sectorY; j < (sectorY + sectorH); j += Configuration.SECTOR_SKIP) {
                color = new Color(fullScreen.getRGB(i, j));
                r += color.getRed();
                g += color.getGreen();
                b += color.getBlue();
                loops++;
            }
        }
        return new Color(r / loops, g / loops, b / loops);
    }

    public int getLed() {
        return led;
    }

    public void setLed(int led) {
        this.led = led;
    }

    public boolean isStartSector() {
        return startSector;
    }

    public void setStartSector(boolean startSector) {
        this.startSector = startSector;
    }

    public int getSkip() {
        return skip;
    }

    public void setSkip(int skip) {
        this.skip = skip;
    }

    public boolean isVertical() {
        return vertical;
    }

    public void setVertical(boolean vertical) {
        this.vertical = vertical;
    }

    @Override
    public String toString() {
        return "Sector{" +
                "led=" + led +
                ", skip=" + skip +
                ", startSector=" + startSector +
                ", x="+getX()+", y="+getY()+"}";
    }
}
