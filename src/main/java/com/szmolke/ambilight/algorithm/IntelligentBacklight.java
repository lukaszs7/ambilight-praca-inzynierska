package com.szmolke.ambilight.algorithm;

import com.szmolke.ambilight.utils.AlertManager;
import com.szmolke.ambilight.utils.SectorManager;
import jssc.SerialPort;
import jssc.SerialPortException;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import com.szmolke.ambilight.utils.ScreenManager;

import java.awt.*;
import java.awt.image.BufferedImage;

/**
 * Created by Łukasz on 02.08.2016.
 */
public class IntelligentBacklight implements Runnable {
    private SerialPort serialPort;
    private int delay;
    private Screen monitor;
    private Sector[] sectors;
    private Robot robot;
    private ScreenManager screenManager;
    private SectorManager sectorManager;
    private AlertManager alertManager;
    private int[] buffer;
    private int[] bufferOld;
    private static IntelligentBacklight instance;

    private final static Logger log = LoggerFactory.getLogger(IntelligentBacklight.class);

    private IntelligentBacklight() {
        alertManager = new AlertManager();
        sectorManager = new SectorManager();
        screenManager = new ScreenManager();
        robot = screenManager.getRobot();
        buffer = new int[Configuration.getTotalLedsCount() * 3];
        bufferOld = new int[Configuration.getTotalLedsCount() * 3];
    }

    public static IntelligentBacklight getInstance() {
        if (instance == null) {
            instance = new IntelligentBacklight();
        }
        return instance;
    }

    public void resetSerialPort(String portName, int baudrate) {
        closePort(serialPort);
        serialPort = new SerialPort(portName);
        openPort(serialPort);
        setSerialPortParams(baudrate, SerialPort.DATABITS_8, SerialPort.STOPBITS_1, SerialPort.PARITY_NONE);
    }

    public void initBacklight(int screenIndex, int delay) {
        this.delay = delay;
        this.monitor = screenManager.getProperScreen(screenIndex);
        this.sectors = sectorManager.getSectors();
    }

    private void openPort(SerialPort port) {
        try {
            if (port != null && !port.isOpened()) {
                port.openPort();
            }
        } catch (SerialPortException e) {
            e.printStackTrace();
        }
    }

    private void closePort(SerialPort port) {
        try {
            if (port != null && port.isOpened()) {
                port.closePort();
            }
        } catch (SerialPortException e) {
            e.printStackTrace();
        }
    }

    public void setSerialPortParams(int baudrate, int databits, int stopbits, int parity) {
        try {
            serialPort.setParams(baudrate, databits, stopbits, parity);
        } catch (SerialPortException e) {
            e.printStackTrace();
        }
    }

    public static Color manageContrastCorrection(Color source, double contrastRed, double contrastGreen, double contrastBlue) {
        double factorRed = (259 * (contrastRed + 255)) / (255 * (259 - contrastRed));
        double factorGreen = (259 * (contrastGreen + 255)) / (255 * (259 - contrastGreen));
        double factorBlue = (259 * (contrastBlue + 255)) / (255 * (259 - contrastBlue));
        int newRed = truncateColor(factorRed * (source.getRed() - 128) + 128);
        int newGreen = truncateColor(factorGreen * (source.getGreen() - 128) + 128);
        int newBlue = truncateColor(factorBlue * (source.getBlue() - 128) + 128);
        return new Color(newRed, newGreen, newBlue);
    }

    public static Color manageGammaCorrection(Color source, double gammaRed, double gammaGreen, double gammaBlue) {
        double correctionRed = 1 / gammaRed;
        double correctionGreen = 1 / gammaGreen;
        double correctionBlue = 1 / gammaBlue;
        int newRed = (int) (255 * (Math.pow(((double) source.getRed()) / 255, correctionRed)));
        int newGreen = (int) (255 * (Math.pow(((double) source.getGreen()) / 255, correctionGreen)));
        int newBlue = (int) (255 * (Math.pow(((double) source.getBlue()) / 255, correctionBlue)));
        return new Color(newRed, newGreen, newBlue);
    }

    public static Color manageBrithnessColor(Color source, double brightRed, double brightGreen, double brightBlue) {
        int newRed = (int) (source.getRed() * (brightRed / 100));
        int newGreen = (int) (source.getGreen() * (brightGreen / 100));
        int newBlue = (int) (source.getBlue() * (brightBlue / 100));
        return new Color(newRed, newGreen, newBlue);
    }

    private Color manageMinColor(Color newColor, int minColorValue) {
        int actualR = getProperColorValue(newColor.getRed(), minColorValue);
        int actualG = getProperColorValue(newColor.getGreen(), minColorValue);
        int actualB = getProperColorValue(newColor.getBlue(), minColorValue);
        return new Color(actualR, actualG, actualB);
    }

    private int getProperColorValue(int actualColor, int minColorValue) {
        if (actualColor < minColorValue) {
            return minColorValue;
        } else {
            return actualColor;
        }
    }

    public void sendTestValue(Sector sector) {
        try {
            while (serialPort.readString() == null) ;
            for (int i = 1; i <= Configuration.getTotalLedsCount(); i++) {
                if (sector.getLed() == i) {
                    serialPort.writeInt(255);
                    serialPort.writeInt(255);
                    serialPort.writeInt(255);
                } else {
                    serialPort.writeInt(0);
                    serialPort.writeInt(0);
                    serialPort.writeInt(0);
                }
                Thread.sleep(10);
            }
        } catch (SerialPortException e) {
            alertManager.createAlertInfo(Configuration.ALERT_INFO, "", Configuration.ALERT_PORT_CONT);
            log.error("Port źle utworzony ", e);
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
    }

    public void startStaticColor(Color color) {
        try {
            while (serialPort.readString() == null) ;
            for (int i = 1; i <= Configuration.getTotalLedsCount(); i++) {
                serialPort.writeInt(color.getRed());
                serialPort.writeInt(color.getGreen());
                serialPort.writeInt(color.getBlue());
                Thread.sleep(10);
            }
        } catch (SerialPortException e) {
            alertManager.createAlertInfo(Configuration.ALERT_INFO, "", Configuration.ALERT_PORT_CONT);
            log.error("Port źle utworzony ", e);
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
    }

    private static int truncateColor(double value) {
        if (value < 0) {
            return 0;
        } else if (value > 255) {
            return 255;
        } else {
            return (int) value;
        }
    }

    public void setDelay(int delay) {
        this.delay = delay;
    }

    private void resetBuffers(int actualSector, Color newColor) {
        bufferOld[actualSector * 3] = buffer[actualSector * 3];
        bufferOld[actualSector * 3 + 1] = buffer[actualSector * 3 + 1];
        bufferOld[actualSector * 3 + 2] = buffer[actualSector * 3 + 2];

        buffer[actualSector * 3] = newColor.getRed();
        buffer[actualSector * 3 + 1] = newColor.getGreen();
        buffer[actualSector * 3 + 2] = newColor.getBlue();
    }

    private boolean isAllSectorsActual() {
        boolean isAllActual = true;
        for (int i = 0; i < sectors.length; i++) {
            if (!(bufferOld[i * 3] == buffer[i * 3] && bufferOld[i * 3 + 1] == buffer[i * 3 + 1] && bufferOld[i * 3 + 2] == buffer[i * 3 + 2])) {
                isAllActual = false;
                break;
            }
        }
        return isAllActual;
    }

    private int getNextColor(int fromColor, int toColor) {
        boolean up = false;
        if (fromColor < toColor) {
            up = true;
        }

        if (toColor > fromColor) {
            fromColor += Configuration.EASING_COLOR;
        } else if (toColor < fromColor) {
            fromColor -= Configuration.EASING_COLOR;
        }

        if ((up && fromColor > toColor) || (!up && fromColor < toColor)) {
            fromColor = toColor;
        }

        return fromColor;
    }

    private void sendProperColors() throws InterruptedException, SerialPortException {
        while (!isAllSectorsActual()) {
            for (int i = 0; i < sectors.length; i++) {
                bufferOld[i * 3] = getNextColor(bufferOld[i * 3], buffer[i * 3]);
                bufferOld[i * 3 + 1] = getNextColor(bufferOld[i * 3 + 1], buffer[i * 3 + 1]);
                bufferOld[i * 3 + 2] = getNextColor(bufferOld[i * 3 + 2], buffer[i * 3 + 2]);
                serialPort.writeInt(bufferOld[i * 3] * bufferOld[i * 3] / 255);
                serialPort.writeInt(bufferOld[i * 3 + 1] * bufferOld[i * 3 + 1] / 255);
                serialPort.writeInt(bufferOld[i * 3 + 2] * bufferOld[i * 3 + 2] / 255);
            }
            Thread.sleep(delay);
        }
    }

    private void loadProperColors(BufferedImage screenShot) {
        for (int i = 0; i < sectors.length; i++) {
            Color sectorColor = sectors[i].getAvgColor(screenShot);
            Color newColor = manageBrithnessColor(sectorColor, Configuration.RED_BRIGHT, Configuration.GREEN_BRIGHT, Configuration.BLUE_BRIGHT);
            newColor = manageContrastCorrection(newColor, Configuration.RED_CONTRAST, Configuration.GREEN_CONTRAST, Configuration.BLUE_CONTRAST);
            newColor = manageGammaCorrection(newColor, Configuration.RED_GAMMA, Configuration.GREEN_GAMMA, Configuration.BLUE_GAMMA);
            newColor = manageMinColor(newColor, Configuration.MIN_COLOR_VALUE);
            resetBuffers(i, newColor);
        }
    }

    @Override
    public void run() {
        while (Configuration.RUNNING_FLAG) {
            try {
                String startCode = serialPort.readString();
                if (startCode != Configuration.START_STRING) {
                    BufferedImage screenShot = screenManager.getProperScreenImage(monitor, robot);
                    loadProperColors(screenShot);
                    sendProperColors();
                }
            } catch (SerialPortException e) {
                log.error("Port źle utworzony ", e);
            } catch (InterruptedException e) {
                log.error("Error ", e);
            }
        }
    }
}
