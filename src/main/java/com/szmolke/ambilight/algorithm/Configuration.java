package com.szmolke.ambilight.algorithm;

import jssc.SerialPort;

import java.lang.reflect.Field;
import java.util.ArrayList;
import java.util.List;

/**
 * Created by Łukasz on 24.08.2016.
 */
public class Configuration {
    private static final int[] BAUDRATES = {
            SerialPort.BAUDRATE_110, SerialPort.BAUDRATE_300, SerialPort.BAUDRATE_600, SerialPort.BAUDRATE_1200, SerialPort.BAUDRATE_4800,
            SerialPort.BAUDRATE_9600, SerialPort.BAUDRATE_14400, SerialPort.BAUDRATE_19200, SerialPort.BAUDRATE_38400, SerialPort.BAUDRATE_57600,
            SerialPort.BAUDRATE_115200, SerialPort.BAUDRATE_128000, SerialPort.BAUDRATE_256000
    };

    public static int INIT_DELAY = 5;
    public static int INIT_LEDS_TOP = 14; //14
    public static int INIT_LEDS_RIGHT = 9; //9
    public static int INIT_LEDS_BOTTOM = 12; //12
    public static int INIT_LEDS_LEFT = 9; //9
    public static int INIT_LEDS_MAX = 99;
    public static int INIT_LEDS_MIN = 0;
    public static int SECTOR_WIDTH_TV = 35;
    public static int SECTOR_HEIGHT_TV = 35;
    public static int SECTOR_WIDTH_MAIN = 70;
    public static int SECTOR_HEIGHT_MAIN = 70;
    public static int SECTOR_SKIP = 1;
    public static Screen MONITOR = Screen.SECONDARY;
    public static String PORT_NAME = "";
    public static int BAUDRATE = SerialPort.BAUDRATE_115200;
    public static int START_INDEX_LED = 16;
    public static boolean DIRECTION_CLOCKWISE = false;
    public static Mode MODE = Mode.DYNAMIC;
    public static String PROFILE_NAME = "";
    public static int EASING_COLOR = 7;

    public static int RED_BRIGHT = 100;
    public static double RED_CONTRAST = 0;
    public static double RED_GAMMA = 1;
    public static int GREEN_BRIGHT = 100;
    public static double GREEN_CONTRAST = 0;
    public static double GREEN_GAMMA = 1;
    public static int BLUE_BRIGHT = 100;
    public static double BLUE_CONTRAST = 0;
    public static double BLUE_GAMMA = 1;

    public static final String START_STRING = "START";
    public static final String MODE_STATIC = "Statyczny";
    public static final String MODE_DYNAMIC = "Dynamiczny";
    public static final String DIRECTION_CLOCKWISE_STR = "Zgodnie";
    public static final String DIRECTION_COUNTER_CLOCKWISE_STR = "Przeciwnie";

    public static final String ALERT_TITLE_CONFLICT = "Konflikt";
    public static final String ALERT_CONTENT_PROF_EXISTS = "Profil o podanej nazwie istnieje w bazie. Czy chcesz go nadpisać ?";
    public static final String ALERT_DELETE_PROFILE_TIT = "Usuwanie profilu";
    public static final String ALERT_DELETE_PROFILE_CONT = "Profil o podanej nazwie zostanie usunięty z bazy. Czy chcesz go kontynuować ?";
    public static final String ALERT_DELETE_PROFILE_INFO = "Nie wybrałeś żadnego profilu do usunięcia.";
    public static final String ALERT_SAVE_PROFILE_CONT = "Nie wpisałeś nazwy profilu.";
    public static final String ALERT_SAVE_PROFILE_INFO = "Zapisywanie profilu";
    public static final String ALERT_INFO = "Info";
    public static final String ALERT_PORT_CONT = "Nie wybrano właściwego portu!";

    public static boolean RUNNING_FLAG = true;
    public static int MIN_COLOR_VALUE = 60;

    public static int[] getIntBAUDRATES() {
        return BAUDRATES;
    }

    public static String[] getStringBAUDRATES() {
        final String[] array = new String[BAUDRATES.length];
        for (int i = 0; i < array.length; i++) {
            array[i] = String.valueOf(BAUDRATES[i]);
        }
        return array;
    }

    public static int[] getLedsCount() {
        return new int[]{
                Configuration.INIT_LEDS_TOP,
                Configuration.INIT_LEDS_RIGHT,
                Configuration.INIT_LEDS_BOTTOM,
                Configuration.INIT_LEDS_LEFT
        };
    }

    public static int getTotalLedsCount() {
        return INIT_LEDS_TOP + INIT_LEDS_BOTTOM + INIT_LEDS_LEFT + INIT_LEDS_RIGHT;
    }

    public static int getBaudrate(int index) {
        return BAUDRATES[index];
    }

    public static int getIndexBaudrate(int baudrate) {
        int index = 0;
        for (int i = 0; i < BAUDRATES.length; i++) {
            if (baudrate == BAUDRATES[i]) {
                index = i;
                break;
            }
        }
        return index;
    }


    public static String getConfiguration() {
        return "Configuration{" +
                "LEDS_TOP: " + INIT_LEDS_TOP +
                ", LEDS_RIGHT: " + INIT_LEDS_RIGHT +
                ", LEDS_BOTTOM: " + INIT_LEDS_BOTTOM +
                ", LEDS_LEFT: " + INIT_LEDS_LEFT +
                ", Monitor: " + MONITOR +
                ", Clockwise: " + DIRECTION_CLOCKWISE +
                "}";
    }
}
