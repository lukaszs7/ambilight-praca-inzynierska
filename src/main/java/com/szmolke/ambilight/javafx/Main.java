package com.szmolke.ambilight.javafx;

import javafx.application.Application;
import javafx.application.Platform;
import javafx.fxml.FXMLLoader;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.stage.Stage;
import org.h2.jdbcx.JdbcDataSource;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;


public class Main extends Application {
    //TODO Ambizone. Photoshop logo

    private static final Logger log = LoggerFactory.getLogger(Main.class);
    private static final String DB_USERNAME = "lukasz";
    private static final String DB_PASSWORD = "ambi";
    private static final String DB_URL = "jdbc:h2:~/ambizoneDB"; //;AUTO_SERVER=TRUE
    private static final String RES_MAIN_FXML = "views/main.fxml";
    private static final String RES_STYLES_CSS = "css/styles.css";
    private static final String APPLICATION_NAME = "Ambizone";

    @Override
    public void start(Stage primaryStage) throws Exception {
        log.info("START create primary stage");
        Parent root = FXMLLoader.load(getClass().getClassLoader().getResource(RES_MAIN_FXML));
        primaryStage.setTitle(APPLICATION_NAME);
        Scene scene = new Scene(root);
        scene.getStylesheets().add(RES_STYLES_CSS);
        primaryStage.setScene(scene);
        createDataBaseConfig();
        primaryStage.show();
        primaryStage.setOnCloseRequest(event -> {
            Platform.exit();
            System.exit(0);
        });
        log.info("STOP create primary stage");
    }

    public void createDataBaseConfig() {
        log.info("START create database configuration");
        JdbcDataSource ds = new JdbcDataSource();
        ds.setURL(DB_URL);
        ds.setUser(DB_USERNAME);
        ds.setPassword(DB_PASSWORD);
        log.info("STOP create database configuration");
    }

    public static void main(String[] args) {
        launch(args);
    }
}
