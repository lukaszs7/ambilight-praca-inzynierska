package com.szmolke.ambilight.utils;

import javafx.application.Platform;
import javafx.scene.control.Alert;
import javafx.scene.control.ButtonBar;
import javafx.scene.control.ButtonType;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.util.Optional;

/**
 * Created by Łukasz on 16.11.2016.
 */
public class AlertManager {
    private static final Logger log = LoggerFactory.getLogger(AlertManager.class);

    public Optional<ButtonType> createAlertConfirmation(String title, String header, String content) {
        log.info("START - Create alert confirmation");
        Alert alert = new Alert(Alert.AlertType.CONFIRMATION);
        alert.setTitle(title);
        alert.setHeaderText(header);
        alert.setContentText(content);
        ButtonType btnYes = new ButtonType("Tak", ButtonBar.ButtonData.APPLY);
        ButtonType btnNo = new ButtonType("Nie", ButtonBar.ButtonData.CANCEL_CLOSE);
        alert.getButtonTypes().setAll(btnYes, btnNo);
        log.info("STOP - Create alert confirmation");
        return alert.showAndWait();
    }

    public void createAlertInfo(String title, String header, String content) {
        Alert alert = new Alert(Alert.AlertType.INFORMATION);
        alert.setTitle(title);
        alert.setHeaderText(header);
        alert.setContentText(content);
        alert.showAndWait();
    }
}
