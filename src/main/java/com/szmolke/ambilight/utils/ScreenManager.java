package com.szmolke.ambilight.utils;

import com.szmolke.ambilight.algorithm.Screen;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.awt.*;
import java.awt.image.BufferedImage;

/**
 * Created by Łukasz on 10.10.2016.
 */
public class ScreenManager {
    private final static Logger log = LoggerFactory.getLogger(ScreenManager.class);

    private Rectangle getRectScreen(int screen, int graphicsConfig) {
        GraphicsEnvironment environment = GraphicsEnvironment.getLocalGraphicsEnvironment();
        GraphicsDevice[] screenDevices = environment.getScreenDevices();
        Rectangle rect = null;
        try {
            rect = screenDevices[screen].getConfigurations()[graphicsConfig].getBounds();
        } catch (IndexOutOfBoundsException e) {
            log.debug("Brak urządzenia o podanym indeksie !");
            e.printStackTrace();
        }
        return rect;
    }

    public Screen getProperScreen(int index) {
        if (index == 0) {
            return Screen.PRIMARY;
        } else if (index == 1) {
            return Screen.SECONDARY;
        } else {
            return null;
        }
    }

    public BufferedImage getProperScreenImage(Screen screen, Robot robot) {
        if (screen.equals(Screen.PRIMARY)) {
            return robot.createScreenCapture(getRectScreen(0, 0));
        } else if (screen.equals(Screen.SECONDARY)) {
            return robot.createScreenCapture(getRectScreen(1, 0));
        } else {
            return null;
        }
    }

    public Robot getRobot() {
        Robot robot = null;
        try {
            robot = new Robot();
        } catch (AWTException e) {
            log.debug("Błąd przy tworzeniu robota !");
            e.printStackTrace();
        }
        return robot;
    }
}
