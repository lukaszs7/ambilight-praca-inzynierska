package com.szmolke.ambilight.utils;

import com.szmolke.ambilight.algorithm.Configuration;
import com.szmolke.ambilight.algorithm.Sector;
import com.szmolke.ambilight.controllers.MainController;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import javax.imageio.ImageIO;
import java.awt.*;
import java.awt.image.BufferedImage;
import java.io.File;
import java.io.IOException;
import java.util.Arrays;
import java.util.Collections;
import java.util.stream.Collectors;

/**
 * Created by Łukasz on 10.10.2016.
 */

public class SectorManager {
    private static final Logger log = LoggerFactory.getLogger(SectorManager.class);
    private ScreenManager screenManager;

    public SectorManager() {
        screenManager = new ScreenManager();
    }

    public Sector[] getSectors() {
        log.info("START - getSectors");
        BufferedImage screenShot = screenManager.getProperScreenImage(Configuration.MONITOR, screenManager.getRobot());
        Sector[] sectors = getSectors(screenShot.getWidth(), screenShot.getHeight(), Configuration.SECTOR_WIDTH_MAIN, Configuration.SECTOR_HEIGHT_MAIN, Configuration.getLedsCount());
//        Arrays.asList(sectors).stream().forEach(s-> System.out.println(s));
        log.info("STOP - getSectors");
        return sectors;
    }

    public Sector[] getSectors(double imageW, double imageH, int sectorW, int sectorH, int[] ledsCount) {
        Sector[] rectanglesTop = getRectHorrizontally(imageW, imageH, ledsCount[0], true, sectorH);
        Sector[] rectanglesRight = getRectVertically(imageW, imageH, ledsCount[1], false, sectorW);
        Sector[] rectanglesBottom = getRectHorrizontally(imageW, imageH, ledsCount[2], false, sectorH);
        Sector[] rectanglesLeft = getRectVertically(imageW, imageH, ledsCount[3], true, sectorW);
        Sector[] sectors;
        if(Configuration.DIRECTION_CLOCKWISE) {
            Collections.reverse(Arrays.asList(rectanglesBottom));
            Collections.reverse(Arrays.asList(rectanglesLeft));
            sectors = concatListRects(rectanglesTop, rectanglesRight, rectanglesBottom, rectanglesLeft);
        } else {
            Collections.reverse(Arrays.asList(rectanglesTop));
            Collections.reverse(Arrays.asList(rectanglesRight));
            sectors = concatListRects(rectanglesBottom, rectanglesRight, rectanglesTop, rectanglesLeft);
        }
        setStartSector(rectanglesBottom);
        int startIndex = getSectorsStartIndex(sectors);
        setNumbersToRectangles(sectors, startIndex);
        sortSectors(sectors);
        return sectors;
    }

    public void setSectorStyleHover(Sector sector) {
        sector.getStyleClass().clear();
        sector.getStyleClass().add("rectangleHoverStyle");
    }

    public void resetStyle(Sector sector) {
        sector.getStyleClass().clear();
        if(sector.isVertical()) {
            sector.getStyleClass().add("rectangleVerticallyStyle");
        } else {
            sector.getStyleClass().add("rectangleHorrizontallyStyle");
        }
    }

    private void setNumbersToRectangles(Sector[] rectangles, int startIndex) {
        boolean endList = false;
        int startInt = startIndex;
        int j = 0;
        for (int i = startInt; !endList || i < startInt; i++, j++) {
            Sector actual = rectangles[j];
            actual.setLed(i);
            if(i == rectangles.length) {
                i = 0;
                endList = true;
            }
        }
    }

    private void setStartSector(Sector[] sectorsBottom) {
        int indexSector = sectorsBottom.length / 2;
        sectorsBottom[indexSector].setStartSector(true);
    }

    private int getSectorsStartIndex(Sector[] sectors) {
        int index = 0;
        for (int i = 0; i < sectors.length; i++) {
            if(sectors[i].isStartSector()) {
                index = i;
                break;
            }
        }
        return sectors.length - index + 1;
    }

    private void sortSectors(Sector[] sectors) {
        Arrays.sort(sectors, (o1, o2) -> o1.getLed() - o2.getLed());
    }

    private Sector[] getRectHorrizontally(double imageW, double imageH, int countLeds, boolean top, int sectorH) {
        Sector[] sectors = new Sector[countLeds];

        double step = imageW / countLeds;
        for (int i = 0; i < countLeds; i++) {
            Sector rect;
            if (top) {
                rect = new Sector(i * step, 0, step, sectorH, false);
            } else {
                rect = new Sector(i * step, imageH - sectorH, step, sectorH, false);
            }
            rect.setSkip(Configuration.SECTOR_SKIP);
            rect.getStyleClass().add("rectangleHorrizontallyStyle");
            sectors[i] = rect;
        }
        return sectors;
    }

    private Sector[] getRectVertically(double imageW, double imageH, int countLeds, boolean left, int sectorW) {
        Sector[] sectors = new Sector[countLeds];

        double step = imageH / countLeds;
        for (int i = 0; i < countLeds; i++) {
            Sector rect;
            if (left) {
                rect = new Sector(0, i * step, sectorW, step, true);
            } else {
                rect = new Sector(imageW - sectorW, i * step, sectorW, step, true);
            }
            rect.setSkip(Configuration.SECTOR_SKIP);
            rect.getStyleClass().add("rectangleVerticallyStyle");
            sectors[i] = rect;
        }
        return sectors;
    }

    public Sector[] concatListRects(Sector[]... nodes) {
        return Arrays.asList(nodes)
                .stream()
                .flatMap(Arrays::stream)
                .collect(Collectors.toList())
                .toArray(new Sector[getVarArgsConcatLength(nodes)]);
    }

    private int getVarArgsConcatLength(Sector[]... nodes) {
        int sum = 0;
        for (int i = 0; i < nodes.length; i++) {
            sum += nodes[i].length;
        }
        return sum;
    }
}
