package com.szmolke.ambilight.utils;

import javafx.scene.paint.*;

import java.awt.*;
import java.awt.Color;
import java.math.BigDecimal;
import java.math.RoundingMode;

/**
 * Created by Łukasz on 18.11.2016.
 */
public class UtilsManager {
    public double truncateDouble(double value) {
        if (value > 0) {
            return value;
        } else {
            return 0.01;
        }
    }

    public double roundDouble(double value, int precise) {
        return BigDecimal.valueOf(value)
                .setScale(precise, RoundingMode.HALF_UP)
                .doubleValue();
    }

    public Color convertColorFXToAwt(javafx.scene.paint.Color fxColor) {
        java.awt.Color awtColor = new java.awt.Color((float) fxColor.getRed(),
                (float) fxColor.getGreen(),
                (float) fxColor.getBlue(),
                (float) fxColor.getOpacity());
        return awtColor;
    }
}
