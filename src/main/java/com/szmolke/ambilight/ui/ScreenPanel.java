package com.szmolke.ambilight.ui;

/**
 * Created by Łukasz on 05.10.2016.
 */

import com.szmolke.ambilight.algorithm.Configuration;
import com.szmolke.ambilight.algorithm.Screen;
import com.szmolke.ambilight.algorithm.Sector;
import com.szmolke.ambilight.utils.ScreenManager;
import javafx.scene.text.Text;

import java.awt.*;
import java.awt.image.BufferedImage;
import javax.swing.JFrame;
import javax.swing.JPanel;

public class ScreenPanel extends JPanel{

    private Sector[] sectors;
    private JFrame frame;
    private ScreenManager screenManager;

    public ScreenPanel(Sector[] sectors)  {
        this.sectors = sectors;
        screenManager = new ScreenManager();
    }

    @Override
    public void paintComponent(Graphics g) {
        g.setColor(Color.RED);
        g.setFont(new Font("default", Font.BOLD, 25));
        for (int i = 0; i < sectors.length; i++) {
            g.drawRect((int)sectors[i].getX(), (int)sectors[i].getY(), (int)sectors[i].getWidth(), (int)sectors[i].getHeight());
        }
        Text[] sectorTexts = getTexts(sectors);
        for (int i = 0; i < sectorTexts.length; i++) {
            g.drawString(sectorTexts[i].getText(), (int)sectorTexts[i].getX(), (int)sectorTexts[i].getY());
        }
    }

    private Text[] getTexts(Sector[] rectangles) {
        Text[] numbers = new Text[rectangles.length];
        for (int i = 0; i < rectangles.length; i++) {
            Sector actual = rectangles[i];
            numbers[i] = new Text(actual.getX() + actual.getWidth()/2 - 5, actual.getY() + actual.getHeight()/2, String.valueOf(actual.getLed()));
            numbers[i].getStyleClass().add("textMainSectorsStyle");
        }
        return numbers;
    }

    public void startScreenPanel() {
        frame = new JFrame();
        frame.add(this);
        frame.setMinimumSize(new Dimension());
        frame.setExtendedState(JFrame.MAXIMIZED_BOTH);
        frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
        frame.setUndecorated(true);
        frame.getContentPane().setBackground(new Color(1.0f,1.0f,1.0f,0.0f));
        frame.setBackground(new Color(1.0f,1.0f,1.0f,0.0f));
        frame.setVisible(true);
        if(Configuration.MONITOR.equals(Screen.SECONDARY)) {
            BufferedImage image = screenManager.getProperScreenImage(Screen.PRIMARY, screenManager.getRobot());
           // frame.setLocation(image.getWidth(), );
        }
    }

    public void stopScreenPanel() {
        frame.setVisible(false);
        frame.dispose();
    }
}