package com.szmolke.ambilight.ui;

import com.szmolke.ambilight.algorithm.Sector;
import com.szmolke.ambilight.algorithm.Configuration;
import com.szmolke.ambilight.utils.SectorManager;
import javafx.event.EventHandler;
import javafx.scene.image.Image;
import javafx.scene.image.ImageView;
import javafx.scene.input.MouseEvent;
import javafx.scene.layout.Pane;
import javafx.scene.layout.StackPane;
import javafx.scene.text.Text;

import java.util.Arrays;

/**
 * Created by Łukasz on 02.08.2016.
 */
public class CanvasUI {
    private StackPane canvas;
    private int[] ledsCount;
    private SectorManager sectorManager;
    private AddCallback<Sector> addCallback;

    public CanvasUI(int[] ledsCount) {
        sectorManager = new SectorManager();
        this.ledsCount = ledsCount;
        initCanvas();
    }

    public void initCanvas() {
        canvas = new StackPane();
        ImageView imageTV = getImageViewTV();
        Pane rectanglesPane = new Pane();
        Pane imagePane = new Pane();
        Pane numbersPane = new Pane();
        Sector[] sectorsOnImage = sectorManager.getSectors(imageTV.getImage().getWidth(), imageTV.getImage().getHeight(), Configuration.SECTOR_WIDTH_TV, Configuration.SECTOR_HEIGHT_TV, ledsCount);
        rectanglesPane.getChildren().addAll(sectorsOnImage);
        imagePane.getChildren().add(imageTV);
        Text[] numbers = getTexts(sectorsOnImage);
        numbersPane.getChildren().addAll(numbers);
        canvas.getChildren().add(imagePane);
        canvas.getChildren().add(rectanglesPane);
        canvas.getChildren().add(numbersPane);
        canvas.setOnMouseMoved(getMouseListenerHover(sectorsOnImage));
        canvas.setOnMouseClicked(getMouseListenerClick(sectorsOnImage));
    }

    public void setAddCallback(AddCallback<Sector> addCallback) {
        this.addCallback = addCallback;
    }

    private EventHandler<? super MouseEvent> getMouseListenerClick(Sector[] sectors) {
        return mouseEvent ->
                Arrays.asList(sectors)
                        .stream()
                        .forEach(sector -> {
                            if (isEventInSector(mouseEvent, sector)) {
                                addCallback.apply(sector);
                            }
                        });
    }

    private EventHandler<? super MouseEvent> getMouseListenerHover(final Sector[] sectors) {
        return mouseEvent ->
                Arrays.asList(sectors)
                        .stream()
                        .forEach(sector -> {
                            if (isEventInSector(mouseEvent, sector)) {
                                sectorManager.setSectorStyleHover(sector);
                            } else {
                                sectorManager.resetStyle(sector);
                            }
                        });
    }

    private boolean isEventInSector(MouseEvent mouseEvent, Sector sector) {
        return mouseEvent.getX() > sector.getX() && mouseEvent.getX() < (sector.getX() + sector.getWidth()) &&
                mouseEvent.getY() > sector.getY() && mouseEvent.getY() < (sector.getY() + sector.getHeight());
    }

    private Text[] getTexts(Sector[] rectangles) {
        Text[] numbers = new Text[rectangles.length];
        for (int i = 0; i < rectangles.length; i++) {
            Sector actual = rectangles[i];
            numbers[i] = new Text(actual.getX() + actual.getWidth() / 2 - 5, actual.getY() + actual.getHeight() / 2, String.valueOf(actual.getLed()));
            numbers[i].getStyleClass().add("textVerticallyStyle");
        }
        return numbers;
    }

    public ImageView getImageViewTV() {
        return new ImageView(new Image("img/tvBlack.jpg"));
    }

    public StackPane getCanvas() {
        return canvas;
    }

    @FunctionalInterface
    public interface AddCallback<One> {
        void apply(One one);
    }
}
