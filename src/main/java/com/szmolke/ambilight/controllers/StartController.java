package com.szmolke.ambilight.controllers;

import com.szmolke.ambilight.algorithm.IntelligentBacklight;
import com.szmolke.ambilight.algorithm.Mode;
import com.szmolke.ambilight.algorithm.Screen;
import com.szmolke.ambilight.algorithm.Sector;
import com.szmolke.ambilight.javafx.Main;
import com.szmolke.ambilight.algorithm.Configuration;
import com.szmolke.ambilight.utils.ScreenManager;
import com.szmolke.ambilight.utils.SectorManager;
import com.szmolke.ambilight.utils.UtilsManager;
import javafx.collections.FXCollections;
import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.scene.control.*;
import javafx.scene.control.Button;
import javafx.scene.control.Label;
import javafx.scene.control.TextField;
import javafx.scene.layout.AnchorPane;
import javafx.scene.text.Font;
import jssc.SerialPortList;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.net.URL;
import java.util.ResourceBundle;
import java.util.stream.IntStream;

/**
 * Created by Łukasz on 12.10.2016.
 */

public class StartController implements Initializable {
    @FXML
    private AnchorPane mainStart;
    @FXML
    private TextField delayTextField;
    @FXML
    private Button startAlgorithm, stopAlgorithm;
    @FXML
    private ToggleGroup typeDirection;
    @FXML
    private ChoiceBox portsList, monitorsList, baudrateList, choiceMode;
    @FXML
    private ColorPicker choiceColor;
    @FXML
    private Spinner spinnerTop, spinnerLeft, spinnerRight, spinnerBottom;
    @FXML
    private Slider delaySlider, sliderRedBright, sliderGreenBright, sliderBlueBright, sliderRedContrast, sliderGreenContrast, sliderBlueContrast, sliderRedGamma, sliderGreenGamma, sliderBlueGamma;
    @FXML
    private Label labelRedBright, labelGreenBright, labelBlueBright, labelRedContrast, labelGreenContrast, labelBlueContrast, labelRedGamma, labelGreenGamma, labelBlueGamma;
    @FXML
    private RadioButton radioClockwise, radioCounterClockwise;

    private static final Logger log = LoggerFactory.getLogger(MainController.class);

    private Thread startThread;
    private IntelligentBacklight backlight;
    private Slider[] colorsSliders = {sliderRedBright, sliderRedContrast, sliderRedGamma, sliderBlueBright, sliderBlueContrast, sliderBlueGamma, sliderGreenBright, sliderGreenContrast, sliderGreenGamma};
    private Label[] colorsLabels = {labelRedBright, labelRedContrast, labelRedGamma, labelBlueBright, labelBlueContrast, labelBlueGamma, labelGreenBright, labelGreenContrast, labelGreenGamma};
    private UtilsManager utilsManager;
    boolean started = false;

    @Override
    public void initialize(URL location, ResourceBundle resources) {
        utilsManager = new UtilsManager();
        backlight = IntelligentBacklight.getInstance();
        startThread = new Thread(backlight);
        initButtons();
        initChoiceboxes();
        initTextFields();
        initSliders();
        initSpinners();
        initRadioButtons();
        initImageCorrectionComponents();
    }

    private void initSpinners() {
        SpinnerValueFactory valueFactoryTop = new SpinnerValueFactory.IntegerSpinnerValueFactory(Configuration.INIT_LEDS_MIN, Configuration.INIT_LEDS_MAX, Configuration.INIT_LEDS_TOP);
        SpinnerValueFactory valueFactoryRight = new SpinnerValueFactory.IntegerSpinnerValueFactory(Configuration.INIT_LEDS_MIN, Configuration.INIT_LEDS_MAX, Configuration.INIT_LEDS_RIGHT);
        SpinnerValueFactory valueFactoryBottom = new SpinnerValueFactory.IntegerSpinnerValueFactory(Configuration.INIT_LEDS_MIN, Configuration.INIT_LEDS_MAX, Configuration.INIT_LEDS_BOTTOM);
        SpinnerValueFactory valueFactoryLeft = new SpinnerValueFactory.IntegerSpinnerValueFactory(Configuration.INIT_LEDS_MIN, Configuration.INIT_LEDS_MAX, Configuration.INIT_LEDS_LEFT);
        spinnerTop.setValueFactory(valueFactoryTop);
        spinnerRight.setValueFactory(valueFactoryRight);
        spinnerBottom.setValueFactory(valueFactoryBottom);
        spinnerLeft.setValueFactory(valueFactoryLeft);
        spinnerTop.valueProperty().addListener((observable, oldValue, newValue) -> {
            Configuration.INIT_LEDS_TOP = (int) newValue;
        });
        spinnerRight.valueProperty().addListener((observable, oldValue, newValue) -> {
            Configuration.INIT_LEDS_RIGHT = (int) newValue;
        });
        spinnerBottom.valueProperty().addListener((observable, oldValue, newValue) -> {
            Configuration.INIT_LEDS_BOTTOM = (int) newValue;
        });
        spinnerLeft.valueProperty().addListener((observable, oldValue, newValue) -> {
            Configuration.INIT_LEDS_LEFT = (int) newValue;
        });
    }

    private void initRadioButtons() {
        typeDirection.selectedToggleProperty().addListener((observable, oldValue, newValue) -> {
            if(radioClockwise.isSelected()) {
                Configuration.DIRECTION_CLOCKWISE = true;
            } else {
                Configuration.DIRECTION_CLOCKWISE = false;
            }
        });
        if(!Configuration.DIRECTION_CLOCKWISE) {
            radioCounterClockwise.setSelected(true);
        }
    }

    private void initSliders() {
        delaySlider.setValue(Configuration.INIT_DELAY);
        delaySlider.valueProperty().addListener((observable, oldValue, newValue) -> {
            int value = (int) delaySlider.getValue();
            delayTextField.setText(String.valueOf(value));
            Configuration.INIT_DELAY = newValue.intValue();
            if (backlight != null) {
                backlight.setDelay(value);
            }
        });
        sliderRedBright.valueProperty().addListener((observable, oldValue, newValue) -> {
            Configuration.RED_BRIGHT = newValue.intValue();
            setSliderListenerValues(newValue.intValue(), sliderRedBright, labelRedBright);
        });
        sliderRedContrast.valueProperty().addListener((observable, oldValue, newValue) -> {
            Configuration.RED_CONTRAST = utilsManager.roundDouble(newValue.doubleValue(), 2);
            setSliderListenerValues(newValue.doubleValue(), sliderRedContrast, labelRedContrast);
        });
        sliderRedGamma.valueProperty().addListener((observable, oldValue, newValue) -> {
            double value = utilsManager.roundDouble(utilsManager.truncateDouble(newValue.doubleValue()), 2);
            Configuration.RED_GAMMA = value;
            setSliderListenerValues(value, sliderRedGamma, labelRedGamma);
        });
        sliderGreenBright.valueProperty().addListener((observable, oldValue, newValue) -> {
            Configuration.GREEN_BRIGHT = newValue.intValue();
            setSliderListenerValues(newValue.intValue(), sliderGreenBright, labelGreenBright);
        });
        sliderGreenContrast.valueProperty().addListener((observable, oldValue, newValue) -> {
            Configuration.GREEN_CONTRAST = utilsManager.roundDouble(newValue.doubleValue(), 2);
            setSliderListenerValues(newValue.doubleValue(), sliderGreenContrast, labelGreenContrast);
        });
        sliderGreenGamma.valueProperty().addListener((observable, oldValue, newValue) -> {
            double value = utilsManager.roundDouble(utilsManager.truncateDouble(newValue.doubleValue()), 2);
            Configuration.GREEN_GAMMA = value;
            setSliderListenerValues(value, sliderGreenGamma, labelGreenGamma);
        });
        sliderBlueBright.valueProperty().addListener((observable, oldValue, newValue) -> {
            Configuration.BLUE_BRIGHT = newValue.intValue();
            setSliderListenerValues(newValue.intValue(), sliderBlueBright, labelBlueBright);
        });
        sliderBlueContrast.valueProperty().addListener((observable, oldValue, newValue) -> {
            Configuration.BLUE_CONTRAST = utilsManager.roundDouble(newValue.doubleValue(), 2);
            setSliderListenerValues(newValue.doubleValue(), sliderBlueContrast, labelBlueContrast);
        });
        sliderBlueGamma.valueProperty().addListener((observable, oldValue, newValue) -> {
            double value = utilsManager.roundDouble(utilsManager.truncateDouble(newValue.doubleValue()), 2);
            Configuration.BLUE_GAMMA = value;
            setSliderListenerValues(value, sliderBlueGamma, labelBlueGamma);
        });
    }

    public void initImageCorrectionComponents() {
        setSliderListenerValues(Configuration.RED_BRIGHT, sliderRedBright, labelRedBright);
        setSliderListenerValues(Configuration.GREEN_BRIGHT, sliderGreenBright, labelGreenBright);
        setSliderListenerValues(Configuration.BLUE_BRIGHT, sliderBlueBright, labelBlueBright);
        setSliderListenerValues(Configuration.RED_CONTRAST, sliderRedContrast, labelRedContrast);
        setSliderListenerValues(Configuration.GREEN_CONTRAST, sliderGreenContrast, labelGreenContrast);
        setSliderListenerValues(Configuration.BLUE_CONTRAST, sliderBlueContrast, labelBlueContrast);
        setSliderListenerValues(Configuration.RED_GAMMA, sliderRedGamma, labelRedGamma);
        setSliderListenerValues(Configuration.GREEN_GAMMA, sliderGreenGamma, labelGreenGamma);
        setSliderListenerValues(Configuration.BLUE_GAMMA, sliderBlueGamma, labelBlueGamma);
    }

    private void setSliderListenerValues(double value, Slider slider, Label label) {
        slider.setValue(value);
        label.setText((Double.toString(value)).format("%.2f", value));
    }

    private void initTextFields() {
        delayTextField.setText(String.valueOf(Configuration.INIT_DELAY));
    }

    private void initChoiceboxes() {
        String[] ports = SerialPortList.getPortNames();
        portsList.setItems(FXCollections.observableArrayList(ports));
        portsList.getSelectionModel().select(getProperSelectIndex(ports));
        portsList.getSelectionModel().selectedIndexProperty().addListener((observable, oldValue, newValue) -> {
            Configuration.PORT_NAME = SerialPortList.getPortNames()[(int) newValue];
        });

        monitorsList.setItems(FXCollections.observableArrayList(Screen.values()));
        monitorsList.getSelectionModel().select(Configuration.MONITOR.equals(Screen.PRIMARY) ? 0 : 1);
        monitorsList.getSelectionModel().selectedIndexProperty().addListener((observable, oldValue, newValue) -> {
            Configuration.MONITOR = (int) newValue == 0 ? Screen.PRIMARY : Screen.SECONDARY;
        });

        baudrateList.setItems(FXCollections.observableArrayList(Configuration.getStringBAUDRATES()));
        baudrateList.getSelectionModel().select(Configuration.getIndexBaudrate(Configuration.BAUDRATE));
        baudrateList.getSelectionModel().selectedIndexProperty().addListener((observable, oldValue, newValue) -> {
            Configuration.BAUDRATE = Configuration.getBaudrate((int) newValue);
        });

        choiceMode.setItems(FXCollections.observableArrayList(Mode.getTranslateValues()));
        choiceMode.getSelectionModel().select(Configuration.MODE.equals(Mode.DYNAMIC) ? 0 : 1);
        choiceMode.getSelectionModel().selectedIndexProperty().addListener((observable, oldValue, newValue) -> {
            Configuration.MODE = (int) newValue == 0 ? Mode.DYNAMIC : Mode.STATIC;
            checkChoiceColorDisable();
        });
        checkChoiceColorDisable();
    }

    private int getProperSelectIndex(String[] ports) {
        return IntStream.range(0, ports.length)
                .filter(i -> ports[i].equals(Configuration.PORT_NAME))
                .findFirst()
                .orElse(0);
    }

    private void checkChoiceColorDisable() {
        if (Configuration.MODE.equals(Mode.DYNAMIC)) {
            choiceColor.setDisable(true);
        } else {
            choiceColor.setDisable(false);
        }
    }

    private void initButtons() {
        startAlgorithm.setOnMouseClicked(event -> {
            startAlgorithm();
        });
        stopAlgorithm.setOnMouseClicked(event -> {
            stopAlgorithm();
        });
    }

    private void stopAlgorithm() {
        if(started) {
            Configuration.RUNNING_FLAG = false;
            try {
                startThread.join();
            } catch (InterruptedException e) {
                log.error("Error: ", e);
                e.printStackTrace();
            }
            resetSelectedPort();
            started = false;
        }
    }

    private void startAlgorithm() {
        log.debug("START CREATION ALGORITHM");
        int delay = Integer.parseInt(delayTextField.getText());
        int screenIndex = monitorsList.getSelectionModel().getSelectedIndex();
        backlight.initBacklight(screenIndex, delay);
        if(!started && Configuration.MODE.equals(Mode.DYNAMIC)) {
            runDynamicMode();
        }
        if(Configuration.MODE.equals(Mode.STATIC)) {
            runStaticMode();
        }
        resetSelectedPort();
        log.debug("STOP CREATION ALGORITHM");
    }

    public void resetSelectedPort() {
        int baudrateIndex = baudrateList.getSelectionModel().getSelectedIndex();
        int baudrate = Configuration.getIntBAUDRATES()[baudrateIndex];
        String portName = (String) portsList.getSelectionModel().getSelectedItem();
        backlight.resetSerialPort(portName, baudrate);
    }

    public void runDynamicMode() {
        Configuration.RUNNING_FLAG = true;
        resetSelectedPort();
        startThread = new Thread(backlight);
        startThread.start();
        started = true;
    }

    public void runStaticMode() {
        resetSelectedPort();
        backlight.startStaticColor(utilsManager.convertColorFXToAwt(choiceColor.getValue()));
    }

    public IntelligentBacklight getBacklightToTester() {
        int screenIndex = monitorsList.getSelectionModel().getSelectedIndex();
        int delay = Integer.parseInt(delayTextField.getText());
        backlight.initBacklight(screenIndex, delay);
        resetSelectedPort();
        return backlight;
    }

    public TextField getDelayTextField() {
        return delayTextField;
    }

    public ChoiceBox getPortsList() {
        return portsList;
    }

    public ChoiceBox getMonitorsList() {
        return monitorsList;
    }

    public ChoiceBox getBaudrateList() {
        return baudrateList;
    }

    public AnchorPane getMainStart() {
        return mainStart;
    }
}
