package com.szmolke.ambilight.controllers;

import com.szmolke.ambilight.repository.*;
import com.szmolke.ambilight.algorithm.Configuration;
import com.szmolke.ambilight.utils.AlertManager;
import javafx.collections.FXCollections;
import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.scene.control.*;
import javafx.scene.layout.AnchorPane;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.net.URL;
import java.util.*;
import java.util.stream.IntStream;

/**
 * Created by Łukasz on 26.10.2016.
 */
public class ProfileController implements Initializable {
    @FXML
    private AnchorPane mainProfile;
    @FXML
    private Label profileName, serial, baudrate, delay, monitor, direction, ledsTop, ledsRight, ledsBottom, ledsLeft, ledsTotal, mode,
            redBright, redContrast, redGamma, greenBright, greenContrast, greenGamma, blueBright, blueContrast, blueGamma;
    @FXML
    private Button buttonSaveProfile, buttonReadProfile, buttonDeleteProfile;
    @FXML
    private ChoiceBox choiceProfile;
    @FXML
    private TextField textNameProfile;

    private static final Logger log = LoggerFactory.getLogger(ProfileController.class);
    private DatabaseConnector databaseConnector;
    private RepositoryManager repositoryManager;
    private AlertManager alertManager;
    private Label[] colorsLabels;
    private Label[] allLabels;

    @Override
    public void initialize(URL location, ResourceBundle resources) {
        repositoryManager = new RepositoryManager();
        alertManager = new AlertManager();
        databaseConnector = new DatabaseConnector();
        colorsLabels = new Label[]{redBright, redContrast, redGamma, greenBright, greenContrast, greenGamma, blueBright, blueContrast, blueGamma};
        allLabels = new Label[]{profileName, serial, baudrate, delay, monitor, direction, ledsTop, ledsRight, ledsBottom, ledsLeft, ledsTotal, mode,
                redBright, redContrast, redGamma, greenBright, greenContrast, greenGamma, blueBright, blueContrast, blueGamma};
        initButtons();
        initChoiceBoxes();
        String profile = loadProfileNames();
        readProfile(profile, false);
    }

    private void initChoiceBoxes() {
        choiceProfile.valueProperty().addListener((observable, oldValue, newValue) -> {
            String profileName = (String)newValue;
            readProfile(profileName, false);
        });
    }

    public void initButtons() {
        buttonSaveProfile.setOnMouseClicked(event -> createProfile());
        buttonReadProfile.setOnMouseClicked(event -> readProfile((String) choiceProfile.getSelectionModel().getSelectedItem(), true));
        buttonDeleteProfile.setOnMouseClicked(event -> deleteProfile((String) choiceProfile.getSelectionModel().getSelectedItem()));
    }

    public void createProfile() {
        if (textNameProfile.getText() != null && textNameProfile.getText().trim().length() > 0) {
            SettingsEntity settingsEntity = repositoryManager.createProperSettingsEntity(textNameProfile.getText());
            List<SectorEntity> sectors = repositoryManager.createSectorEntitiesSettings(settingsEntity);
            if (!databaseConnector.isProfileExists(settingsEntity)) {
                for (SectorEntity sector : sectors) {
                    databaseConnector.createEntity(sector);
                }
                Configuration.PROFILE_NAME = textNameProfile.getText();
                String profileName = loadProfileNames();
                readProfile(profileName, false);
            } else {
                Optional<ButtonType> op = alertManager.createAlertConfirmation(Configuration.ALERT_TITLE_CONFLICT, "", Configuration.ALERT_CONTENT_PROF_EXISTS);
                if (op.get().getButtonData().equals(ButtonBar.ButtonData.APPLY)) {
                    updateRepository(textNameProfile.getText());
                    String profileName = loadProfileNames();
                    readProfile(profileName, false);
                } else {
                    log.debug("Profile was not updated");
                }
            }
        } else {
            alertManager.createAlertInfo(Configuration.ALERT_SAVE_PROFILE_INFO,"",Configuration.ALERT_SAVE_PROFILE_CONT);
            log.debug("Profile name should have value");
        }
    }

    public void deleteProfile(final String selectedProfile) {
        log.debug("START DELETING PROFILE");
        if(choiceProfile.getSelectionModel().isEmpty()) {
            alertManager.createAlertInfo(Configuration.ALERT_DELETE_PROFILE_TIT,"",Configuration.ALERT_DELETE_PROFILE_INFO);
        } else {
            Optional<ButtonType> op = alertManager.createAlertConfirmation(Configuration.ALERT_DELETE_PROFILE_TIT, "", Configuration.ALERT_DELETE_PROFILE_CONT);
            if (op.get().getButtonData().equals(ButtonBar.ButtonData.APPLY)) {
                SettingsEntity toDelete = databaseConnector.findSettingsByProfileName(selectedProfile);
                databaseConnector.deleteProfile(toDelete);

                loadProfileNames();
                resetLabels();
                resetConnection();
            } else {
                log.debug("Profile was not deleted");
            }
        }
    }

    public void readProfile(final String selectedProfile, boolean assignConfiguration) {
        if (selectedProfile != null) {
            SettingsEntity settingsEntity = databaseConnector.findSettingsByProfileName(selectedProfile);
            log.debug("\n LOADED PROFILE: Name: " + selectedProfile + ", " + settingsEntity.toString());
            assignSettingsEntityValues(settingsEntity, assignConfiguration);
            assignColorsEntityValues(settingsEntity, assignConfiguration);
        } else {
            resetLabels();
        }
    }

    private void assignColorsEntityValues(SettingsEntity settingsEntity, boolean assignConfiguration) {
        double[] colors = {settingsEntity.getColors().getRedBright(), settingsEntity.getColors().getRedContrast(), settingsEntity.getColors().getRedGamma(),
                settingsEntity.getColors().getGreenBright(), settingsEntity.getColors().getGreenContrast(), settingsEntity.getColors().getGreenGamma(),
                settingsEntity.getColors().getBlueBright(), settingsEntity.getColors().getBlueContrast(), settingsEntity.getColors().getBlueGamma()};
        double[] configuration = {Configuration.RED_BRIGHT, Configuration.RED_CONTRAST, Configuration.RED_GAMMA,
                Configuration.GREEN_BRIGHT, Configuration.GREEN_CONTRAST, Configuration.GREEN_GAMMA,
                Configuration.BLUE_BRIGHT, Configuration.BLUE_CONTRAST, Configuration.BLUE_GAMMA};
        IntStream.range(0, colors.length).forEach(i -> {
            colorsLabels[i].setText(String.valueOf(colors[i]));
            if (assignConfiguration) {
                configuration[i] = colors[i];
            }
        });
    }

    private void resetLabels() {
        Arrays.asList(allLabels).stream().forEach(label -> label.setText(""));
    }

    private void assignSettingsEntityValues(SettingsEntity settingsEntity, boolean assignConfiguration) {
        profileName.setText(settingsEntity.getProfileName());
        serial.setText(settingsEntity.getSerialPort());
        baudrate.setText(String.valueOf(settingsEntity.getBaudrate()));
        delay.setText(String.valueOf(settingsEntity.getInitDelay()));
        mode.setText(settingsEntity.getMode().getText());
        monitor.setText(settingsEntity.getMonitor().getText());
        direction.setText(settingsEntity.isClockwise() ? Configuration.DIRECTION_CLOCKWISE_STR : Configuration.DIRECTION_COUNTER_CLOCKWISE_STR);
        ledsTop.setText(String.valueOf(settingsEntity.getInitLedsTop()));
        ledsRight.setText(String.valueOf(settingsEntity.getInitLedsRight()));
        ledsBottom.setText(String.valueOf(settingsEntity.getInitLedsBottom()));
        ledsLeft.setText(String.valueOf(settingsEntity.getInitLedsLeft()));
        ledsTotal.setText(String.valueOf(settingsEntity.getInitLedsTop() + settingsEntity.getInitLedsRight() + settingsEntity.getInitLedsBottom() + settingsEntity.getInitLedsLeft()));
        if(assignConfiguration) {
            Configuration.PORT_NAME = settingsEntity.getSerialPort();
            Configuration.BAUDRATE = settingsEntity.getBaudrate();
            Configuration.INIT_DELAY = settingsEntity.getInitDelay();
            Configuration.MODE = settingsEntity.getMode();
            Configuration.MONITOR = settingsEntity.getMonitor();
            Configuration.DIRECTION_CLOCKWISE = settingsEntity.isClockwise();
            Configuration.INIT_LEDS_TOP = settingsEntity.getInitLedsTop();
            Configuration.INIT_LEDS_RIGHT = settingsEntity.getInitLedsRight();
            Configuration.INIT_LEDS_BOTTOM = settingsEntity.getInitLedsBottom();
            Configuration.INIT_LEDS_LEFT = settingsEntity.getInitLedsLeft();
            Configuration.PROFILE_NAME = settingsEntity.getProfileName();
        }
    }

    public String loadProfileNames() {
        List<String> names = databaseConnector.findAllProfileNames();
        choiceProfile.setItems(FXCollections.observableArrayList(names));
        if(Configuration.PROFILE_NAME != "") {
            choiceProfile.getSelectionModel().select(Configuration.PROFILE_NAME);
        }
        return (String) this.choiceProfile.getSelectionModel().getSelectedItem();
    }

    public void updateRepository(String profileName) {
        log.debug("Profile update");
        SettingsEntity settingsFromDB = databaseConnector.findSettingsByProfileName(textNameProfile.getText());
        databaseConnector.deleteSectorsWithSetId(settingsFromDB.getId());
        ColorEntity colors = repositoryManager.createColorEntitySettings();
        databaseConnector.updateColorEntity(settingsFromDB.getColors().getId(), colors);
        SettingsEntity newSettings = repositoryManager.createProperSettingsEntity(profileName);
        databaseConnector.updateSettingsEntity(settingsFromDB.getId(), settingsFromDB.getColors().getId(), newSettings);
        List<SectorEntity> sectors = repositoryManager.createSectorEntitiesSettings(settingsFromDB);
        for (SectorEntity sector : sectors) {
            databaseConnector.createEntity(sector);
        }
        resetConnection();
    }

    public void resetConnection() {
        databaseConnector.closeEntityManager();
        databaseConnector = new DatabaseConnector();
    }

    public AnchorPane getMainProfile() {
        return mainProfile;
    }
}
