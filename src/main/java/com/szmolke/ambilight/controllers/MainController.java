package com.szmolke.ambilight.controllers;

import com.szmolke.ambilight.algorithm.IntelligentBacklight;
import com.szmolke.ambilight.algorithm.Sector;
import com.szmolke.ambilight.ui.CanvasUI;
import com.szmolke.ambilight.utils.SectorManager;
import javafx.application.Platform;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.fxml.Initializable;
import javafx.geometry.Bounds;
import javafx.geometry.Point2D;
import javafx.scene.Node;
import javafx.scene.Parent;
import javafx.scene.control.Button;
import javafx.scene.input.MouseEvent;
import javafx.scene.layout.AnchorPane;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.io.IOException;
import java.net.URL;
import java.util.Arrays;
import java.util.ResourceBundle;

public class MainController implements Initializable {
    @FXML
    private AnchorPane content;
    @FXML
    private Button buttonTester, buttonStart, buttonExit, buttonProfile;

    private static final String RES_TESTER_FXML = "/views/tester.fxml";
    private static final String RES_START_FXML = "/views/start.fxml";
    private static final String RES_PROFILE_FXML = "/views/profile.fxml";
    private static final Logger log = LoggerFactory.getLogger(MainController.class);
    private StartController startController;
    private TesterController testerController;
    private ProfileController profileController;

    @Override
    public void initialize(URL location, ResourceBundle resources) {
        startController = getStartController();
        testerController = getTesterController();
        profileController = getProfileController();
        initButtons();
    }

    private void initButtons() {
        buttonTester.setOnMouseClicked(event -> {
            IntelligentBacklight backlightToTester = startController.getBacklightToTester();
            testerController.getCanvasUI().setAddCallback(backlightToTester::sendTestValue);
            resetViewToFXML(RES_TESTER_FXML);
        });
        buttonStart.setOnMouseClicked(event -> resetViewToFXML(RES_START_FXML));
        buttonProfile.setOnMouseClicked(event -> resetViewToFXML(RES_PROFILE_FXML));
        buttonExit.setOnMouseClicked(event -> {
            Platform.exit();
            System.exit(0);
        });
    }

    private void resetViewToFXML(String fxmlResource) {
        content.getChildren().clear();
        switch (fxmlResource) {
            case RES_START_FXML:
                content.getChildren().setAll(startController.getMainStart());
                break;
            case RES_PROFILE_FXML:
                content.getChildren().setAll(profileController.getMainProfile());
                break;
            case RES_TESTER_FXML:
                content.getChildren().setAll(testerController.getMainTester());
                break;
        }
    }

    private ProfileController getProfileController() {
        return getLoader(RES_PROFILE_FXML).getController();
    }

    private TesterController getTesterController() {
        return getLoader(RES_TESTER_FXML).getController();
    }

    private StartController getStartController() {
        return getLoader(RES_START_FXML).getController();
    }

    private FXMLLoader getLoader(String fxml) {
        FXMLLoader fxmlLoader = new FXMLLoader();
        try {
            fxmlLoader.load(getClass().getResource(fxml).openStream());
        } catch (IOException e) {
            log.error("ERROR: ", e);
            e.printStackTrace();
        }
        return fxmlLoader;
    }
}
