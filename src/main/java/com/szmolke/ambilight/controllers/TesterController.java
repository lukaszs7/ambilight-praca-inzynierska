package com.szmolke.ambilight.controllers;

import com.szmolke.ambilight.algorithm.Sector;
import com.szmolke.ambilight.ui.CanvasUI;
import com.szmolke.ambilight.ui.ScreenPanel;
import com.szmolke.ambilight.algorithm.Configuration;
import com.szmolke.ambilight.utils.SectorManager;
import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.scene.control.Button;
import javafx.scene.layout.AnchorPane;
import javafx.scene.layout.StackPane;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.net.URL;
import java.util.ResourceBundle;

/**
 * Created by Łukasz on 14.10.2016.
 */
public class TesterController implements Initializable {
    @FXML
    private AnchorPane mainTester;
    @FXML
    private StackPane canvasPane;
    @FXML
    private Button showSectors;
    @FXML
    private CanvasUI canvasUI;

    private boolean openMainSectors = false;
    private Sector[] sectors;
    private ScreenPanel screenPanel;
    private SectorManager sectorManager;

    private static final Logger log = LoggerFactory.getLogger(TesterController.class);

    @Override
    public void initialize(URL location, ResourceBundle resources) {
        initCanvas(Configuration.getLedsCount());
        initButtons();
        sectorManager = new SectorManager();
        sectors = sectorManager.getSectors();
    }

    private void initButtons() {
        showSectors.setOnMouseClicked(event -> {
            if (!openMainSectors) {
                showSectors(sectors);
            } else {
                if(screenPanel != null) {
                    screenPanel.stopScreenPanel();
                }
            }
            openMainSectors = !openMainSectors;
        });
    }

    private void initCanvas(int[] ledsCount) {
        canvasUI = new CanvasUI(ledsCount);
        canvasPane.getChildren().add(canvasUI.getCanvas());
    }

    private void showSectors(Sector[] sectors) {
        screenPanel = new ScreenPanel(sectors);
        screenPanel.startScreenPanel();
    }

    public CanvasUI getCanvasUI() {
        return canvasUI;
    }

    public AnchorPane getMainTester() {
        return mainTester;
    }
}
